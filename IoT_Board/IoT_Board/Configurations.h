#ifndef CS_MF_CONF_H_
#define CS_MF_CONF_H_

#include "Array_Ref.h"

constexpr int NUMBER_OF_STATIONS = 4;
constexpr int NUMBER_OF_CURRENT_CHANNELS = 5;

constexpr int CALF_SESSION_TIMEOUT = 60;
constexpr int STATUS_TIME_PERIOD = 30;
constexpr int SECOND_STATUS_TIME = 120;
constexpr int SENSOR_RESET_PERIOD = 10;
constexpr int DISPENSE_DELAY = 3;
constexpr int COM_PACKET_TIMEOUT = 120;

constexpr int RFID_BUFFER_SIZE = 32;
constexpr int FEED_MAILBOX_SIZE = 4;
constexpr int LOG_MAILBOX_SIZE = 4;
constexpr int STATUS_MAILBOX_SIZE = 2;

constexpr int REBOOT_FAIL_COUNT = 3;
constexpr int DISPENSE_FAIL_LOCKOUT_COUNT = 5;
constexpr int STALL_LOW_COUNT = 4;
constexpr int STALL_HIGH_COUNT = 1;
constexpr int RFID_READS = 2;

constexpr String_Ref Host_Name{ "trailer.calfsmart.com"}
;
constexpr int Port_Number = 5050;
constexpr String_Ref Feed_Url{ "/api/server/v2/meal/feed/request" }
;
constexpr String_Ref Log_Url{ "/api/server/v2/meal/feed/log" }
;
constexpr String_Ref Status_Url{ "/api/server/v2/meal/asset/state" }
;

//-- Bit masks for incoming control packets
constexpr int CTRL_ENTICEMENT = 0x01;
constexpr int CTRL_RESTART = 0x02;
constexpr int CTRL_RFID_BEEP = 0x04;


constexpr int TIME_FF = 9;

constexpr char DBG_ERR[] = { "\n\e[0;41m\e[1;37m" };
constexpr char DBG_NRM[] = { "\e[39;49m\n\n" };

//-- Global functions / variables
//-- This is probably not ideal and may not be threadsafe.
void chirp(float time);
void debug_if(bool, const char*, ...);


#endif

