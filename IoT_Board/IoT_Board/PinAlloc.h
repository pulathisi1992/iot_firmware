/* cs_pins.h
 *
 * Defines for the pins used on the calfsmart board
 *
 * Author: Tobin Hall
 * email: tobin.g.hall@gmail.com
 */

#ifndef CS_PINS_H_
#define CS_PINS_H_

#include "PinNames.h"

// constexpr - constant specifier
//MODEM PINS
constexpr PinName MODEM_PG = PC_8;
constexpr PinName MODEM_RES = PC_9;
constexpr PinName MODEM_EN = PA_8;
constexpr PinName MODEM_TX = PA_9;
constexpr PinName MODEM_RX = PA_10;
constexpr PinName MODEM_RTS = PA_12;
constexpr PinName MODEM_CTS = PA_11;

//Jtag Programmer Pins
constexpr PinName STD_UART_TX = PC_6;
constexpr PinName STD_UART_RX = PC_7;
constexpr PinName STD_UART_SYSSWDIO = PA_13;
constexpr PinName STD_UART_SYSSWCLK = PA_14;

//Solar Controller inputs
//constexpr PinName SOLAR_RE = ; //SOLAR_DE invertered is SOLAR_RE
constexpr PinName SOLAR_DE = PD_12;
constexpr PinName SOLAR_TX = PD_13;
constexpr PinName SOLAR_RX = PD_14;

//DC motor pins
constexpr PinName AUGUR_PIN1 = PB_5;
constexpr PinName AUGUR_PIN2 = PB_6;
constexpr PinName AUGUR_PIN3 = PB_7;
constexpr PinName AUGUR_PIN4 = PE_0;
constexpr PinName AUGUR_PIN5 = PE_1;

//DC motor enable feedback pin
constexpr PinName MOTOR_OC1 = PF_2;
constexpr PinName MOTOR_OC2 = PF_3;
constexpr PinName MOTOR_OC3 = PC_4;
constexpr PinName MOTOR_OC4 = PC_5;
constexpr PinName MOTOR_OC5 = PB_0;

//DC motor current sense RESET pin
constexpr PinName MOTOR_CURRENT_RESET = PC_10;
//DC motor current measurement pin
constexpr PinName MOTOR_C_SENSE1 = PC_1;
constexpr PinName MOTOR_C_SENSE2 = PC_2;
constexpr PinName MOTOR_C_SENSE3 = PC_3;
constexpr PinName MOTOR_C_SENSE4 = PA_0;
constexpr PinName MOTOR_C_SENSE5 = PA_1;


//Valve pins
constexpr PinName VALVE_PIN1 = PD_6;
constexpr PinName VALVE_PIN2 = PD_5;
constexpr PinName VALVE_PIN3 = PD_4;
constexpr PinName VALVE_PIN4 = PD_3;
constexpr PinName VALVE_PIN5 = PD_2;
constexpr PinName VALVE_PIN6 = PD_1;
constexpr PinName VALVE_PIN7 = PD_0;
constexpr PinName VALVE_PIN8 = PC_12;
constexpr PinName VALVE_PIN9 = PC_11;

//Valve enable feedback pin
constexpr PinName V_FEEDBACK_PIN = PD_7;
//Valve current sense RESET pin
constexpr PinName VALVE_CURRENT_RESET = PB_3;
//Valve current measurement pin
constexpr PinName VALVE_C_SENSE = PC_0;

//Hatch sensor also names as SAFETY_INTERLOCK
constexpr PinName INTERLOCK_INPUT = PB_1;

//Meal Sensor ANALOGUE 0-10V INPUTS
constexpr PinName MEAL_SENSE_PIN1 = PA_7;
constexpr PinName MEAL_SENSE_PIN2 = PA_6;
constexpr PinName MEAL_SENSE_PIN3 = PA_5;
constexpr PinName MEAL_SENSE_PIN4 = PA_4;
constexpr PinName MEAL_SENSE_PIN5 = PA_3;
constexpr PinName MEAL_SENSE_PIN6 = PA_2;

//Meal Sensor Teach Pins
constexpr PinName MEAL_TEACH_PIN1 = PC_13;
constexpr PinName MEAL_TEACH_PIN2 = PE_6;
constexpr PinName MEAL_TEACH_PIN3 = PE_5;
constexpr PinName MEAL_TEACH_PIN4 = PE_4;
constexpr PinName MEAL_TEACH_PIN5 = PE_3;
constexpr PinName MEAL_TEACH_PIN6 = PE_2;

//RFID Pins
constexpr int RFID_BAUD_RATE = 9600;

constexpr PinName RFID_SERIAL1_TX = PB_10;
constexpr PinName RFID_SERIAL1_RX = PB_11;
constexpr PinName RFID_SERIAL2_TX = PE_10;
constexpr PinName RFID_SERIAL2_RX = PE_11;
constexpr PinName RFID_SERIAL3_TX = PE_8;
constexpr PinName RFID_SERIAL3_RX = PE_9;
constexpr PinName RFID_SERIAL4_TX = PF_9;
constexpr PinName RFID_SERIAL4_RX = PF_10;

//RFID Enable pins
constexpr PinName RFID_ENABLE_PIN1 = PE_12;
constexpr PinName RFID_ENABLE_PIN2 = PE_13;
constexpr PinName RFID_ENABLE_PIN3 = PE_14;  // <<<<<< 14
constexpr PinName RFID_ENABLE_PIN4 = PE_15;

//SD card
constexpr PinName SD_CS = PB_12;
constexpr PinName SD_MOSI = PB_13;
constexpr PinName SD_SCLK = PB_14;
constexpr PinName SD_MISO = PB_15;
constexpr PinName SD_DET = PF_11;

//LED
constexpr PinName LED_POWER = PB_2;
constexpr PinName LED_CELLULAR = PD_8;
constexpr PinName LED_AUGUR1 = PD_9;
constexpr PinName LED_AUGUR2 = PD_10;
constexpr PinName LED_AUGUR3 = PD_11;
constexpr PinName LED_AUGUR4 = PD_15;
constexpr PinName FUTURE1 = PF_6;
constexpr PinName FUTURE2 = PA_15;
	
//Buzzer
constexpr PinName CHIRP_PIN = PB_4;

//Function Button
constexpr PinName FUNCTION_BUTTON = PE_7;

//I2C
constexpr PinName I2CSDA = PB_9;
constexpr PinName RESET_INPUT = PB_8;

//constexpr PinName STATUS_LIGHT = ;



#endif /* cs_pins*/

