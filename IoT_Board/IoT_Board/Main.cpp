
//#include "PinNames-F091VC.h"
//#include "PeripheralNames.h"
#include <mbed.h>
#include <rtos.h>

#include <cstdarg>
#include <string>

#include "PinAlloc.h"
#include "Communications.h"
#include "RFID.h"
#include "Operations.h"
#include "Monitoring.h"
#include "Configurations.h"

#define DBG 1
#define DBG_PFX "MAIN: "

RawSerial pc(STD_UART_TX, STD_UART_RX, 115200);

//----	Threads
Thread thread_com(osPriorityAboveNormal, 2048);
Thread thread_rfid(osPriorityNormal, 2048);
Thread thread_manager(osPriorityNormal, 2048);
Thread thread_monitor(osPriorityNormal, 2048);

DigitalOut chirpPin(CHIRP_PIN);

//----	Declaration of object/pin arrays:
//----		This is a quick and dirty way to avoid type issues
DigitalOut _auger_en[] = { AUGUR_PIN1, AUGUR_PIN2, AUGUR_PIN3, AUGUR_PIN4, AUGUR_PIN5 };
DigitalIn _auger_on[] = { MOTOR_OC1, MOTOR_OC2, MOTOR_OC3, MOTOR_OC4, MOTOR_OC5 };
AnalogIn _motor_sense[] = { MOTOR_C_SENSE1, MOTOR_C_SENSE2, MOTOR_C_SENSE3, MOTOR_C_SENSE4, MOTOR_C_SENSE5 };
//DigitalIn _meal_sensor[] = { MEAL_SENSE_PIN1, MEAL_SENSE_PIN2, MEAL_SENSE_PIN3, MEAL_SENSE_PIN4, MEAL_SENSE_PIN5, MEAL_SENSE_PIN6 };
AnalogIn meal_sensor[] = { MEAL_SENSE_PIN1, MEAL_SENSE_PIN2, MEAL_SENSE_PIN3, MEAL_SENSE_PIN4 };
DigitalOut _meal_sensor_en[] = { NC, NC, NC, NC, NC, NC };  //-Feature not available

DigitalOut _Teach[] = { MEAL_TEACH_PIN1, MEAL_TEACH_PIN2, MEAL_TEACH_PIN3, MEAL_TEACH_PIN4, MEAL_TEACH_PIN5, MEAL_TEACH_PIN6 };
//DigitalOut Status_light = { STATUS_LIGHT };
DigitalOut _MotorCurrentSenseReset = MOTOR_CURRENT_RESET;
DigitalOut _ValveCurrentSenseReset = VALVE_CURRENT_RESET;

RawSerial _rfid_serial[] = {
	{ RFID_SERIAL1_TX, RFID_SERIAL1_RX, RFID_BAUD_RATE },
	{ RFID_SERIAL2_TX, RFID_SERIAL2_RX, RFID_BAUD_RATE },
	{ RFID_SERIAL3_TX, RFID_SERIAL3_RX, RFID_BAUD_RATE },
	{ RFID_SERIAL4_TX, RFID_SERIAL4_RX, RFID_BAUD_RATE }

};

DigitalOut _rfid_en[] = { RFID_ENABLE_PIN1, RFID_ENABLE_PIN2, RFID_ENABLE_PIN3, RFID_ENABLE_PIN4 };
DigitalOut _led_en[] = { LED_POWER, LED_CELLULAR, LED_AUGUR1, LED_AUGUR2, LED_AUGUR3, LED_AUGUR4, FUTURE1, FUTURE2 };

DigitalIn _safety_interlock(INTERLOCK_INPUT); // - Feature not available
//DigitalOut safety_interlock_en(INTERLOCK_ENABLE); - Feature not available

//----	Objects
Cs_mf_rfid rfid(_rfid_serial, _rfid_en);
Cs_mf_manager manager(_auger_en, _auger_on, meal_sensor, _meal_sensor_en, &_safety_interlock, _motor_sense, _led_en);
//Cs_mf_com com(_led_en);
Cs_mf_com com;
cs_mf_monitor monitor(_auger_en, &_safety_interlock);

Timeout chirpT;

void stopChirp(DigitalOut *aug) {
	aug->write(false);
}
void chirp(float time) {
	chirpPin.write(true);
	chirpT.attach(callback(stopChirp, &chirpPin), time);
}

void debug_if(bool dbg_mode, const char* fmt, ...) //added this here to display debugging through serial
{
	if (dbg_mode)
	{
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}

//static_assert(STDIO_UART_TX == PC_6, "The STDIO UART must be altered in PeripheralNames.h");





 int main() {

	//Status_light.write(true);
	 _led_en[0].write(true);



	_ValveCurrentSenseReset.write(true);
	wait_ms(100);
	_ValveCurrentSenseReset.write(false);

	_MotorCurrentSenseReset.write(true);
	wait_ms(100);
	_MotorCurrentSenseReset.write(false);

	///////_safety_interlock
	chirp(1.0);
	//////-- Link up all the mailboxes:
	rfid.setRfidMailbox(&manager.rfid_mailbox);
	com.setFeedMailbox(&manager.feed_mailbox);
	monitor.setStatusMailbox(&com.status_mailbox);
	manager.setLogMailbox(&com.log_mailbox);
	manager.setRequestMailbox(&com.request_mailbox);

	monitor.setLockoutStatus(&manager.station_lockout_status);
	//////-- Start the threads:
	thread_com.start(callback(com.run, &com));
	thread_rfid.start(callback(rfid.run, &rfid));
	thread_manager.start(callback(manager.run, &manager));
	thread_monitor.start(callback(monitor.run, &monitor));

}
