/*
 * cs_mf_monitor.cpp
 *
 *  Created on: 15/08/2017
 *      Author: Tobin
 */

#include "Monitoring.h"
#include "PinAlloc.h"
#include "Thread.h"

namespace
{

}

#define DBG 1
#define DBG_PFX "\e[1;35mMNTR: "

cs_mf_monitor::cs_mf_monitor(DigitalOut * auger_en, DigitalIn * safety_interlock)
: enable_sd(NC)
, enable_i2c(NC)
, enable_external_serial(NC) //There was a pin here before
, solar_de(SOLAR_DE)
//,solar_re(SOLAR_RE)
, reset_switch(RESET_INPUT)
, solar(SOLAR_TX, SOLAR_RX, 115200)
{
	//-- Disable all unused peripherals
	//enable_sd.write(0);
	//enable_i2c.write(0);
	enable_external_serial.write(0);

	//-- Get the start time for uptime calculations
	init_time = time(NULL);
	status_time = 0;
	status_mailbox = NULL;
	lockout_status = NULL;
	reset_switch.mode(PullUp);

	_safety_interlock = safety_interlock;
	_auger_en = auger_en;
}

void cs_mf_monitor::run(cs_mf_monitor* monitor) {
	debug_if(DBG, DBG_PFX"Starting\r\n");
	while (1) {
		//-- Check if a status packet is due to be sent
		if(time(NULL) < 1) {
			monitor->send_status(monitor);
		}
		if ((time(NULL) - monitor->init_time) > 20 && (time(NULL) - monitor->init_time) < 22) {
			monitor->send_status(monitor);
		}
		if ((time(NULL) - monitor->init_time) > SECOND_STATUS_TIME && (time(NULL) - monitor->init_time) < (SECOND_STATUS_TIME + 2)) {
			monitor->send_status(monitor);
		}
		if ((time(NULL) - monitor->status_time) > STATUS_TIME_PERIOD) {
			monitor->send_status(monitor);
		}

		if (monitor->_safety_interlock->read() == 1 && 0) {
			for (uint8_t station = 0; station < NUMBER_OF_STATIONS; station++) {
				monitor->_auger_en[station].write(0);
			}
			debug_if(DBG, DBG_PFX"HATCH OPEN!\r\n");
			chirp(0.15);
			Thread::wait(200);
			chirp(0.15);
			Thread::wait(5000);
		}
		else
			Thread::wait(1500);

		if (monitor->reset_switch.read() == 0)
		{
			debug_if(DBG, DBG_PFX"GOT RESET COMMAND!\r\n");
			DigitalOut boot0(PF_11);
			boot0.write(0);
			wait_us(100);
			NVIC_SystemReset();
		}

	}
}


uint8_t cs_mf_monitor::read_battery_level() {
	float vAverage = 0.0;
	for (uint8_t i = 0; i < 10; i++) {
		//vAverage += voltage_sense.read();
		Thread::wait(100);
	}
	vAverage /= 10;
	//-- Convert to voltage:
	vAverage = (vAverage * 20.8) - 2.7;

	vAverage = (vAverage - 11.0) * 85.0;
	debug_if(DBG, DBG_PFX"Got Battery Voltage %u\r\n", (uint8_t)vAverage);
	return (uint8_t) vAverage;
}


void cs_mf_monitor::send_status(cs_mf_monitor* monitor) {

	debug_if(DBG, DBG_PFX"Sending Status Packet\r\n");
	monitor->status_time = time(NULL);
	status_t * status_mail = monitor->status_mailbox->alloc(0);
	if (status_mail == NULL) {
		debug_if(DBG, DBG_PFX"Could not allocate mail\r\n");
	}
	else {
		//-- Populate the status mail packet
		status_mail->uptime = time(NULL) - monitor->init_time;
		status_mail->timestamp = time(NULL);

		status_mail->battery_level = monitor->read_register_16(SOLAR_ADDR_BATTERY);
		status_mail->solar_controller_temp = monitor->read_register_16(SOLAR_ADDR_TEMP);
		status_mail->solar_panel_power = monitor->read_register_32(SOLAR_ADDR_PANEL);
		status_mail->solar_consumed_energy = monitor->read_register_32(SOLAR_ADDR_CONSUMED);
		status_mail->solar_generated_energy = monitor->read_register_32(SOLAR_ADDR_GENERATED);

		status_mail->hopper_level = monitor->read_hopper_level();
		status_mail->lockout = *monitor->lockout_status;
		status_mail->hatch_open = (monitor->_safety_interlock->read() == 1 && 0);
		//-- Send the mail packet
		monitor->status_mailbox->put(status_mail);
	}
}


uint8_t cs_mf_monitor::read_hopper_level() {
	//TODO
	return 0;
}

uint32_t cs_mf_monitor::read_register_32(uint16_t regAdd) {
	//-- Set network ID
	uint16_t regCount = 2;
	uint16_t result;
	packet[0] = 1;
	//-- modbus command to read a register
	packet[1] = 4;
	//-- two bytes for register address and register count
	packet[2] = HIGHBYTE(regAdd);
	packet[3] = LOWBYTE(regAdd);
	packet[4] = HIGHBYTE(regCount);
	packet[5] = LOWBYTE(regCount);
	//-- calculate the CRC:
	uint16_t u16CRC = CRC16(packet, 6);
	//append the CRC to the packet
	packet[6] = LOWBYTE(u16CRC);
	packet[7] = HIGHBYTE(u16CRC);

	//-- Send the data
	set_tx(1);
	wait_us(100);
	core_util_critical_section_enter();
	for (int i = 0; i < 8; i++) {
		solar.putc(packet[i]);
	}
	wait_us(200);
	set_tx(0);


	if (wait_for_packet(2 * regCount + 5) == 1) {
		result = (packet[3] << 8) | (packet[4]);
		result |= (packet[5] << 24) | (packet[6] << 16);
	}
	core_util_critical_section_exit();
	return result;
}
uint16_t cs_mf_monitor::read_register_16(uint16_t regAdd) {
	//-- Set network ID
	uint16_t regCount = 2;
	packet[0] = 1;
	//-- modbus command to read a register
	packet[1] = 4;
	//-- two bytes for register address and register count
	packet[2] = HIGHBYTE(regAdd);
	packet[3] = LOWBYTE(regAdd);
	packet[4] = HIGHBYTE(regCount);
	packet[5] = LOWBYTE(regCount);
	//-- calculate the CRC:
	uint16_t u16CRC = CRC16(packet, 6);
	//append the CRC to the packet
	packet[6] = LOWBYTE(u16CRC);
	packet[7] = HIGHBYTE(u16CRC);

	//-- Send the data
	set_tx(true);
	wait_us(100);
	core_util_critical_section_enter();
	for (int i = 0; i < 8; i++) {
		solar.putc(packet[i]);
	}
	wait_us(200);
	set_tx(false);


	if (wait_for_packet(2 * regCount + 5)) {
		core_util_critical_section_exit();
		return (packet[3] << 8) | (packet[4]);
	}
	core_util_critical_section_exit();
	return 0;
}
int8_t cs_mf_monitor::wait_for_packet(uint8_t expected_packet_length) {
	uint8_t received_bytes = 0;
	while (received_bytes < expected_packet_length) {
		uint16_t timeout_count = 0;
		while (!solar.readable()) {
			if (timeout_count++ > 2000) {
				//printf("Timeout waiting for response from solar, received %hu of %hu\r\n", received_bytes, expected_packet_length);
				return false;
			}
			wait_us(10);
		}
		packet[received_bytes] = solar.getc();
		received_bytes++;
	}
	return true;
}


void cs_mf_monitor::set_tx(uint8_t tx) {
	solar_de.write(tx);
	//solar_re.write(tx);
}

uint16_t cs_mf_monitor::CRC16(uint8_t* data, uint16_t data_length) {
	static const uint16_t CRCTable[] = {
		0X0000,0XC0C1,0XC181,0X0140,0XC301,0X03C0,0X0280,0XC241,
		0XC601,0X06C0,0X0780,0XC741,0X0500,0XC5C1,0XC481,0X0440,
        0XCC01,0X0CC0,0X0D80,0XCD41,0X0F00,0XCFC1,0XCE81,0X0E40,
		0X0A00,0XCAC1,0XCB81,0X0B40,0XC901,0X09C0,0X0880,0XC841,
		0XD801,0X18C0,0X1980,0XD941,0X1B00,0XDBC1,0XDA81,0X1A40,
		0X1E00,0XDEC1,0XDF81,0X1F40,0XDD01,0X1DC0,0X1C80,0XDC41,
		0X1400,0XD4C1,0XD581,0X1540,0XD701,0X17C0,0X1680,0XD641,
		0XD201,0X12C0,0X1380,0XD341,0X1100,0XD1C1,0XD081,0X1040,
		0XF001,0X30C0,0X3180,0XF141,0X3300,0XF3C1,0XF281,0X3240,
		0X3600,0XF6C1,0XF781,0X3740,0XF501,0X35C0,0X3480,0XF441,
		0X3C00,0XFCC1,0XFD81,0X3D40,0XFF01,0X3FC0,0X3E80,0XFE41,
		0XFA01,0X3AC0,0X3B80,0XFB41,0X3900,0XF9C1,0XF881,0X3840,
		0X2800,0XE8C1,0XE981,0X2940,0XEB01,0X2BC0,0X2A80,0XEA41,
		0XEE01,0X2EC0,0X2F80,0XEF41,0X2D00,0XEDC1,0XEC81,0X2C40,
		0XE401,0X24C0,0X2580,0XE541,0X2700,0XE7C1,0XE681,0X2640,
		0X2200,0XE2C1,0XE381,0X2340,0XE101,0X21C0,0X2080,0XE041,
		0XA001,0X60C0,0X6180,0XA141,0X6300,0XA3C1,0XA281,0X6240,
		0X6600,0XA6C1,0XA781,0X6740,0XA501,0X65C0,0X6480,0XA441,
		0X6C00,0XACC1,0XAD81,0X6D40,0XAF01,0X6FC0,0X6E80,0XAE41,
		0XAA01,0X6AC0,0X6B80,0XAB41,0X6900,0XA9C1,0XA881,0X6840,
		0X7800,0XB8C1,0XB981,0X7940,0XBB01,0X7BC0,0X7A80,0XBA41,
		0XBE01,0X7EC0,0X7F80,0XBF41,0X7D00,0XBDC1,0XBC81,0X7C40,
		0XB401,0X74C0,0X7580,0XB541,0X7700,0XB7C1,0XB681,0X7640,
		0X7200,0XB2C1,0XB381,0X7340,0XB101,0X71C0,0X7080,0XB041,
		0X5000,0X90C1,0X9181,0X5140,0X9301,0X53C0,0X5280,0X9241,
		0X9601,0X56C0,0X5780,0X9741,0X5500,0X95C1,0X9481,0X5440,
		0X9C01,0X5CC0,0X5D80,0X9D41,0X5F00,0X9FC1,0X9E81,0X5E40,
		0X5A00,0X9AC1,0X9B81,0X5B40,0X9901,0X59C0,0X5880,0X9841,
		0X8801,0X48C0,0X4980,0X8941,0X4B00,0X8BC1,0X8A81,0X4A40,
		0X4E00,0X8EC1,0X8F81,0X4F40,0X8D01,0X4DC0,0X4C80,0X8C41,
		0X4400,0X84C1,0X8581,0X4540,0X8701,0X47C0,0X4680,0X8641,
		0X8201,0X42C0,0X4380,0X8341,0X4100,0X81C1,0X8081,0X4040
	};
	uint16_t wCRCWord = 0xFFFF;

	while (data_length--)
	{
		uint8_t nTemp = *data++ ^ wCRCWord;
		wCRCWord >>= 8;
		wCRCWord ^= CRCTable[nTemp];
	}
	return wCRCWord;
}
