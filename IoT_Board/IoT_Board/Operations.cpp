/*
 * cs_mf_manager.cpp
 *
 *  Created on: 7/08/2017
 *      Author: Tobin
 */

#include <Operations.h>
#include "Configurations.h"
#include "PinAlloc.h"

#define DBG 1
#define DBG_PFX "\033[0;32mMNGR: "

namespace
{

}

int Cs_mf_manager::runningAugers = 0;

Cs_mf_manager::Cs_mf_manager(
	DigitalOut *auger,
	DigitalIn *_auger_on,
	AnalogIn *meal_sensor,
	DigitalOut *meal_sensor_en,
	DigitalIn *safety_interlock,
	AnalogIn *_motor_sense,
	DigitalOut *led_en)
	: driver_cs_disable(NC)
	, _auger_en(auger)
	, auger_on(_auger_on)
	, _meal_sensor(meal_sensor)
	, _safety_interlock(safety_interlock)
	, motor_sense(_motor_sense)
	, led_en(led_en)
{

	log_mailbox = NULL;
	request_mailbox = NULL;
	Cs_mf_manager::runningAugers = 0;
	//sensor_reset_time = 0;
	for(int i = 0 ; i < NUMBER_OF_STATIONS ; i++) {
		stations[i].meal_detected_since_dispense = 1;
	}
}

void Cs_mf_manager::run(Cs_mf_manager* manager) {

	int loop = 0;
	debug_if(DBG, DBG_PFX"Starting\r\n");
	while (1) {
		loop++;
		if (loop % 20 == 0) {
			runningAugers--;
		}

		manager->process_mailboxes();
		for (uint8_t station = 0; station < NUMBER_OF_STATIONS; station++) {
			manager->process_station(station);
		}


		//Thread::wait(500);
		int t = 0;
		int bulk = 0;
		while (t < 500) {
			bulk = manager->limit_current_pull(bulk);
			t += 12;
		}

	}
}

int8_t Cs_mf_manager::process_mailboxes() {
	osEvent evt = rfid_mailbox.get(0);
	if (evt.status == osEventMail) {
		calf_info_t *new_rfid = (calf_info_t *)evt.value.p;
		debug_if(DBG, DBG_PFX"Tag in %u rfid:%lu\r\n", (uint32_t)new_rfid->station, (uint32_t)new_rfid->rfid);
		uint8_t new_station = new_rfid->station;
		if (new_rfid->rfid == 0) {
			end_calf_session(new_station);
			debug_if(DBG, DBG_PFX"Calf is no longer in station %u\r\n", (unsigned int)new_station);
		}
		else {
			for (uint8_t old_station = 0; old_station < NUMBER_OF_STATIONS; old_station++) {
				if (new_station != old_station && new_rfid->rfid == stations[old_station].current_tag) {
					debug_if(DBG, DBG_PFX"Calf moved from station %u to %u during session\r\n", (uint32_t) old_station, (uint32_t) new_station);
					//-- End the session of the calf in the new station
					end_calf_session(new_station);
					//copy the data from that station and zero the current station
					stations[new_station].current_tag = stations[old_station].current_tag;
					stations[new_station].allocated_time = stations[old_station].allocated_time;
					stations[new_station].allocated_units = stations[old_station].allocated_units;
					stations[new_station].dispensed_units = stations[old_station].dispensed_units;
					stations[new_station].last_tag_time = new_rfid->timestamp;
					//-- The calf is no longer in the old station
					stations[old_station].current_tag = 0;
					//-- must set dispensed units to 0 otherwise it will send as unidentified calf
					stations[old_station].dispensed_units = 0;
					end_calf_session(old_station);
				}
			}
			if (new_rfid->rfid != stations[new_station].current_tag) {
				debug_if(DBG, DBG_PFX"New calf in station %u\r\n", (unsigned int)new_station);
				end_calf_session(new_station);
				stations[new_station].current_tag = new_rfid->rfid;
				stations[new_station].last_tag_time = new_rfid->timestamp;
				start_calf_session(new_station);
			}
		}

		rfid_mailbox.free(new_rfid);
	}
	evt = feed_mailbox.get(0);
	if (evt.status == osEventMail) {
		feed_info_t *new_feed = (feed_info_t *)evt.value.p;
		//-- Feed allocations where RFID == 0 and station == 0xFF are actually mode changing commands
		if(new_feed->rfid == 0 && new_feed->station == 0xFF) {
			enticement_mode = (new_feed->auger_units & CTRL_ENTICEMENT) == CTRL_ENTICEMENT;
			enticement_time = new_feed->auger_time;
			rfid_beep_enabled = (new_feed->auger_units & CTRL_RFID_BEEP) == CTRL_RFID_BEEP;

			sensor_calibration[0] = new_feed->sensor_calibration[0];
			sensor_calibration[1] = new_feed->sensor_calibration[1];
			sensor_calibration[2] = new_feed->sensor_calibration[2];
			sensor_calibration[3] = new_feed->sensor_calibration[3];

			debug_if(DBG, DBG_PFX "Changing mode: %u enticement_time: %u control byte: %u\r\n", (uint32_t)enticement_mode, (uint32_t)enticement_time, (uint32_t)new_feed->auger_units);
			debug_if(DBG,
				DBG_PFX "calibration: %u, %u, %u, %u\r\n",
				(uint32_t)sensor_calibration[0],
				(uint32_t)sensor_calibration[1],
				(uint32_t)sensor_calibration[2],
				(uint32_t)sensor_calibration[3]);
		}
		else {
			uint8_t station = new_feed->station;
			if (new_feed->rfid == stations[station].current_tag) {
				stations[station].allocated_time = new_feed->auger_time;
				stations[station].allocated_units = new_feed->auger_units;
			}
			else {
				debug_if(DBG, "%s%s Station: %hu\r\n ", DBG_PFX, DBG_ERR, station);
				debug_if(DBG, "%s%s Received allocation for wrong calf! ", DBG_PFX, DBG_ERR); // %lu %lu %s", , new_feed->rfid, stations[station].current_tag, DBG_NRM);
			}
		}
		feed_mailbox.free(new_feed);
	}
	return 1;
}

int8_t Cs_mf_manager::end_calf_session(uint8_t station) {
	if (stations[station].current_tag != 0 || stations[station].dispensed_units != 0) {
		debug_if(DBG, DBG_PFX"Ending calf session in station %u \r\n", (unsigned int)station);
		feed_info_t * new_request = log_mailbox->alloc(100);
		if (new_request == NULL) {
			debug_if(DBG, "%s%s Could not allocate mail, this means we arent sending log! %llu %llu%s", DBG_PFX, DBG_ERR, DBG_NRM);
			return -1;
		}
		new_request->rfid = stations[station].current_tag;
		new_request->station = station;
		new_request->timestamp = time(NULL);
		new_request->auger_units = stations[station].dispensed_units;
		if (enticement_mode == 1)
			new_request->auger_time = enticement_time;
		else
			new_request->auger_time = stations[station].allocated_time;

		log_mailbox->put(new_request);
	}

	stations[station].current_tag = 0;
	stations[station].allocated_time = 0;
	stations[station].allocated_units = 0;
	stations[station].dispensed_units = 0;
	stations[station].last_tag_time = 0;
	return 1;
}

int8_t Cs_mf_manager::process_station(uint8_t station) {
	//-- Do nothing if in lockout
	if(stations[station].lockout) {
		return -1;
	}

	//-- Is the bowl empty?
	if(meal_sensor_read(station)) {
		if (stations[station].sensor_low_count < STALL_LOW_COUNT) {
			stations[station].sensor_low_count++;
		}
		else {
			 //-- The bowl is definitely empty

			//-- Check if we can dispense

			// never run more than two  augers
      if(runningAugers <= 0) {
				runningAugers = 0;
				_auger_en[4].write(0);
			}

			// limit auger run time
			if(enticement_time > 3000) {
				 // 3s
				//  never run the augers for more than 3s at a time
				debug_if(DBG, "%s %s enticement_time > 3000ms, cannot dispense for that long! %s", DBG_PFX, DBG_ERR, DBG_NRM);
				return -1;
			}

			//-- Check if the bowl was full
			if(stations[station].bowl_full == 1 && stations[station].has_augered == 1) {
				//-- The bowl was full and is now empty. Log the feed.
				stations[station].dispensed_units++;
				stations[station].bowl_full = 0;
				stations[station].has_augered = 0;
				debug_if(DBG, DBG_PFX"Falling edge detected in %d \r\r\n", (int)station);
			}

			if ((time(NULL) - stations[station].last_dispense_time) > DISPENSE_DELAY) {
				//-- Check to make sure something was actually dispensed last time
				if(stations[station].meal_detected_since_dispense == 0) {
					if (stations[station].dispense_fail_count++ < DISPENSE_FAIL_LOCKOUT_COUNT) {
						debug_if(DBG,
							"%s%s Failed to dispense in station %lu, %lu failures, redispensing %s \r\n",
							DBG_PFX,
							DBG_ERR,
							(uint32_t)station,
							(uint32_t)stations[station].dispense_fail_count,
							DBG_NRM);
						dispense(station);
					}
					else {
						lockout(station);
						return -1;
					}
				}
				else if(enticement_mode || (stations[station].dispensed_units < stations[station].allocated_units)) {
					dispense(station);
				}
			}
		}
	}
	else {
		 //-- There is meal detected
		if(stations[station].sensor_high_count < STALL_HIGH_COUNT) {
			stations[station].sensor_high_count++;
		}
		else {
			stations[station].sensor_low_count = 1;
			stations[station].bowl_full = 1;
			stations[station].meal_detected_since_dispense = 1;
			stations[station].dispense_fail_count = 0;
		}
	}
	return 1;
}

int8_t Cs_mf_manager::start_calf_session(uint8_t station) {
	debug_if(DBG, DBG_PFX "Starting calf session in station %d \r\n", (int)station);
	if (rfid_beep_enabled)
		chirp(0.2);
	if (enticement_mode == 0) {
		calf_info_t * new_request = request_mailbox->alloc(0);
		if (new_request == NULL) {
			debug_if(DBG, " %s%s Could not allocate new mail\n %s", DBG_PFX, DBG_ERR, DBG_NRM);
			return -1;
		}
		new_request->rfid = stations[station].current_tag;
		new_request->station = station;
		new_request->timestamp = stations[station].last_tag_time;

		request_mailbox->put(new_request);
	}
	return 1;
}

void Cs_mf_manager::dispense(uint8_t station) {

	//-- run the vibrator at the same time as the augers
  DigitalOut reset = MOTOR_CURRENT_RESET;
	runningAugers++;
	reset.write(1);
	Thread::wait(1);
	reset.write(0);
	Thread::wait(2);
	_auger_en[4].write(1);
	Thread::wait(500);
	//-- Dispense the feed, enable the auger
	_auger_en[station].write(1);


	// Auger has run
	stations[station].has_augered = 1;


	//-- Attach the timeout to disable the auger
	if(enticement_mode) {

		// if the auger time is too small it will lockout immediately
		if(enticement_time < 250) {
			enticement_time = 250;
		}

		// stop vibrator after (enticement_time+100 ms )
		vibrtor_timeout.attach(callback(stop_vibrator, &_auger_en[4]), (enticement_time / 1000.0) + 0.1);

		// stop auger after (enticement_time ms)
		stations[station].auger_timeout.attach(callback(stop_auger, &_auger_en[station]), enticement_time / 1000.0);
		debug_if(DBG, "%s station:%lu, Dispensing for %lu ms, %lu units, Enticement mode\r\n", DBG_PFX, (uint32_t)station, enticement_time, (uint32_t)stations[station].dispensed_units);
	}
	else {

		// if the auger time is too small it will lockout immediately
		if(stations[station].allocated_time < 250) {
			stations[station].allocated_time = 250;
		}

		// stop vibrator after (enticement_time+100 ms )
		vibrtor_timeout.attach(callback(stop_vibrator, &_auger_en[4]), (stations[station].allocated_time / 1000.0) + 0.1);

		// stop auger after (enticement_time ms)
		stations[station].auger_timeout.attach(callback(stop_auger, &_auger_en[station]), stations[station].allocated_time / 1000.0);
		debug_if(DBG, "%s station:%lu, Dispensing for %lu ms, %lu of %lu, Restricted mode\r\n", DBG_PFX, (uint32_t)station, stations[station].allocated_time, (uint32_t)stations[station].dispensed_units, (uint32_t) stations[station].allocated_units);
	}
	stations[station].meal_detected_since_dispense = 0;
	stations[station].last_dispense_time = time(nullptr);
}

bool Cs_mf_manager::meal_sensor_read(int station) {

	uint32_t avg  = 0;
	for (int i = 0; i < 5; i++)
	{
		avg += _meal_sensor[station].read_u16();
		wait_us(1);
	}
	avg /= 5;
	avg++;
	return avg > sensor_calibration[station];
}

bool Cs_mf_manager::lockout(int station) {
	_auger_en[station].write(0);
	runningAugers--;
	_auger_en[4].write(0);
	duty[station] = 1;
	stations[station].lockout = 1;
	debug_if(DBG, "%s%s Locking out station %lu %s \r\n", DBG_PFX, DBG_ERR, (uint32_t)station, DBG_NRM);
	station_lockout_status |= 1 << station;
	led_en[station + 2].write(1);
}

int Cs_mf_manager::limit_current_pull(int killAll) {
	 // 12ms


	//_auger_en[0].write(0);
	//led_en[1+ 2].write(1);
  DigitalOut valves[] = { VALVE_PIN1, VALVE_PIN2, VALVE_PIN3, VALVE_PIN4, VALVE_PIN5, VALVE_PIN6, VALVE_PIN7, VALVE_PIN8, VALVE_PIN8, VALVE_PIN9 };
	valves[0].write(0);
	valves[1].write(0);
	valves[2].write(0);
	valves[3].write(0);
	valves[4].write(0);
	valves[5].write(0);
	valves[6].write(0);
	valves[7].write(0);
	valves[8].write(0);

	// check for short
	for(uint8_t station = 0 ; station < NUMBER_OF_STATIONS ; station++) {
		if (_auger_en[station].read() == 1 && auger_on[station].read() == 1) {
			debug_if(DBG, "%s%s short in station %lu \r\n", DBG_PFX, DBG_ERR, (uint32_t)station);
			lockout(station);
		}
	}

	int _10amps = amp_to_ADC_converter(10) / 10;
	int _20amps = amp_to_ADC_converter(20) / 10;
	int _30amps = amp_to_ADC_converter(40) / 10; // this was increased and not tested.
	int _50amps = amp_to_ADC_converter(52) / 10;

	int reads[] = { 0, 0, 0, 0, 0 };
	int sum = 0;

	Thread::wait(10);
	for (uint8_t channel = 0; channel < NUMBER_OF_CURRENT_CHANNELS; channel++) {
		int r = motor_sense[channel].read_u16() / 30;
		reads[channel] += r;
		sum += r;
	}
	Thread::wait(10);
	for (uint8_t channel = 0; channel < NUMBER_OF_CURRENT_CHANNELS; channel++) {
		int r = motor_sense[channel].read_u16() / 30;
		reads[channel] += r;
		sum += r;
	}
	Thread::wait(10);
	for (uint8_t channel = 0; channel < NUMBER_OF_CURRENT_CHANNELS; channel++) {
		int r = motor_sense[channel].read_u16() / 30;
		reads[channel] += r;
		sum += r;
	}

	if (sum > _30amps) {
		debug_if(DBG, "%s%s %u _30amps Limit hit %uA > %uA \r\n", DBG_PFX, DBG_ERR, killAll, sum / 85, _30amps / 85);
		killAll++;
	}
	else {
		killAll = 0;
	}
	if (killAll > 12 || channel_duty(reads, 4)) {
		 // this was changed from a 4 to a 12
		debug_if(DBG, "%s%s Vibrator Amp Limit hit %uA %u  \r\n", DBG_PFX, DBG_ERR, reads[4] / 850, killAll);
		_auger_en[4].write(0);
		killAll = 13;  //this was changed from 5 to a 13
	}
	for (uint8_t station = 0; station < NUMBER_OF_STATIONS; station++) {
		if (stations[station].lockout == 1) {
			continue;
		}
		// 50A*10ms
		if(reads[station] > _50amps) {
			debug_if(DBG, "%s%s Station:%u _50amps Limit hit %uA > %uA \r\n", DBG_PFX, DBG_ERR, (uint32_t)station, reads[station] / 850, _50amps);
			lockout(station);
			continue;
		}
		// 40A*30ms Total || 10% duty per 50s
		if(killAll > 12 || channel_duty(reads, station)) {
			 // this was changed from a 4 to a 12
			lockout(station);
		}
	}
	return killAll;
}

bool Cs_mf_manager::channel_duty(int reads[], int channel) { 

	int _10amps = amp_to_ADC_converter(10) / 10;
	int _20amps = amp_to_ADC_converter(20) / 10;
	int _30amps = amp_to_ADC_converter(30) / 10;
	int _50amps = amp_to_ADC_converter(52) / 10;

	if (reads[channel] > _50amps) {
		 //was 30amps before
		duty[channel] += 10;
	}
	if (reads[channel] > _30amps) {
		 //was 20amps before
		duty[channel] += 10;
	}
	if (reads[channel] > _20amps) {
		 //was 10amps before
		duty[channel] += 10;
	}
	else {
		if (duty[channel] < 2) {
			duty[channel] = 1;
		}
		duty[channel] -= 1;
	}
	//debug_if(DBG, "%s%s Station:%u %u %u %u\r\n", DBG_PFX, DBG_ERR, (uint32_t)channel, reads[channel], duty[channel], _50amps);

	if(duty[channel] > 41600) {
		 //duty cycle duration (10% limit)
		debug_if(DBG, "%s%s Station:%u Duty Limit hit \r\n", DBG_PFX, DBG_ERR, (uint32_t)channel);
		return true;
	}
	return false;
}


int Cs_mf_manager::amp_to_ADC_converter(int amps) {
	// make sure this function is optimised out
	int amp = 850;
	return amps*amp;
}
