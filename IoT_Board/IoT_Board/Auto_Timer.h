#pragma once
#include <chrono>
#include "Timer.h"

class Auto_Timer
{
public:
	Auto_Timer()
		: _t()
	{
		_t.start();
	}

	void reset()
	{
		_t.reset();
	}

	std::chrono::microseconds read()
	{
		return std::chrono::microseconds(static_cast<int64_t>(_t.read_high_resolution_us()));
	}
private:
	mbed::Timer _t;
};
