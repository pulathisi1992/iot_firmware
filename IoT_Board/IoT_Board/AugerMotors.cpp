#include <mbed.h>
#include <rtos.h>
#include "AugerMotors.h"
#include "PinAlloc.h"




AugerMotors::AugerMotors(PinName hatchDetect, std::array<PinName, 5> augerPins) 
	: hatchOpen(hatchDetect)
	, augerMotors {
	mbed::DigitalOut(augerPins[0]), 
			mbed::DigitalOut(augerPins[1]), 
			mbed::DigitalOut(augerPins[2]), 
			mbed::DigitalOut(augerPins[3]), 
			mbed::DigitalOut(augerPins[4])
}
{
	
}