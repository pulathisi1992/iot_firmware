/*
 * cs_mf_monitor.h
 *
 *  Created on: 15/08/2017
 *      Author: Tobin
 */

#ifndef MEAL_SRC_CS_MF_MONITOR_H_
#define MEAL_SRC_CS_MF_MONITOR_H_


#define HIGHBYTE(x) ((x>>8) & 0xFF)
#define LOWBYTE(x) (x & 0xFF)

//-- http://www.solar-elektro.cz/data/dokumenty/1733_modbus_protocol.pdf
#define SOLAR_ADDR_PANEL 0x3102
#define SOLAR_ADDR_BATTERY 0x3104
#define SOLAR_ADDR_TEMP 0x3112
#define SOLAR_ADDR_CONSUMED 0x330A
#define SOLAR_ADDR_GENERATED 0x3312



#include <mbed.h>
#include "Configurations.h"
#include "CommStructs.h"
#include "Mail.h"


class cs_mf_monitor {
public:
	cs_mf_monitor(DigitalOut * auger_en, DigitalIn * safety_interlock);

	static void run(cs_mf_monitor * monitor);

	void setStatusMailbox(Mail<status_t, STATUS_MAILBOX_SIZE>* statusMailbox) {
		status_mailbox = statusMailbox;
	}

	void setLockoutStatus(volatile uint8_t* lockoutStatus) {
		lockout_status = lockoutStatus;
	}

private:
	Mail<status_t, STATUS_MAILBOX_SIZE> * status_mailbox;

	void send_status(cs_mf_monitor * monitor);
	uint8_t read_battery_level();
	uint8_t read_hopper_level();
	uint32_t read_register_32(uint16_t regAdd);
	uint16_t read_register_16(uint16_t regAdd);
	int8_t wait_for_packet(uint8_t expected_packet_length);

	static uint16_t CRC16(uint8_t *data, uint16_t data_length);
	void set_tx(uint8_t tx);
	uint8_t packet[256];

	DigitalOut enable_sd;
	DigitalOut enable_i2c;
	DigitalOut enable_external_serial;
	//AnalogIn voltage_sense;
	DigitalOut solar_de;
	//DigitalOut solar_re;
	DigitalIn reset_switch;
	RawSerial solar;
	DigitalIn * _safety_interlock;
	DigitalOut * _auger_en;

	uint64_t init_time;
	uint64_t status_time;

	volatile uint8_t * lockout_status;
}
;

#endif /* MEAL_SRC_CS_MF_MONITOR_H_ */
