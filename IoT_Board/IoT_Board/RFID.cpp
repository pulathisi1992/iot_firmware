/*
 * cs_mf_rfid.cpp
 *  Info: https://bitbucket.org/calfsmart/cs_meal_mbed/wiki/RFID.md
 *  Created on: 4/08/2017
 *      Author: Tobin
 */

#include "RFID.h"
#include <mbed.h>
#include <algorithm>
#include <cstring>


#define DBG 1
#define DBG_PFX "\e[0;33mRFID: "

Cs_mf_rfid::Cs_mf_rfid(RawSerial *serial, DigitalOut *enable)
{
	_serial = serial;
	_enable = enable;
	_active_station = 0;
	rfid_mailbox = NULL;
	//-- Power off RFIDs and detach all interrupts:
	for(uint8_t i = 0 ; i < NUMBER_OF_STATIONS ; i++) {
		_enable[i].write(0);
		_serial[i].attach(NULL, Serial::RxIrq);
		_serial[i].attach(NULL, Serial::TxIrq);
	}
}


void Cs_mf_rfid::run(Cs_mf_rfid *rfid) {
	debug_if(DBG, DBG_PFX "Starting\r\n");
	//-- Power on all RFIDs
	for(uint8_t i = 0 ; i < NUMBER_OF_STATIONS ; i++) {
		rfid->_enable[i].write(1);
		rfid->_last_tag[i] = 0;
	}
	//-- Wait for them to power on (~250ms)
	Thread::wait(300);

	//-- Send the Set Reader De-active to all RFIDs
	//-- Disable all LEDs
	for(uint8_t i = 0 ; i < NUMBER_OF_STATIONS ; i++) {
		rfid->_serial[i].puts("SRD\r");
		rfid->_serial[i].puts("SL4\r");
	}

	Thread::wait(1000);  //ADDED

	while(1) {
		//-- Loop through all stations
		for(uint8_t station = 0 ; station < NUMBER_OF_STATIONS * 2 ; station++) {
			rfid->_active_station = station / 2;
			rfid->_process_station(station / 2);
		}
	}
}
void Cs_mf_rfid::_process_station(uint8_t station) {
	_activate_rfid(station);
	Thread::wait(250);
	//-- Wait for tag to be received
	_deactivate_rfid(station);
	uint64_t tag = _extract_tag(station);

	if (tag != 0) {
		_tag_history[station][_history_index[station]] = tag;
		_history_index[station] = (_history_index[station] + 1) % RFID_READS;
		_tag_time[station] = time(NULL);
		//  debug_if(DBG,DBG_PFX"\aDetected tag %llu in %d \r\n",tag, (int)station); //====================================>
		if(tag != _last_tag[station]) {
			//-- Check if we have read this tag enough times to be sure its a valid tag
			if(_check_tag_history(station)) {
				//-- Check if the calf is in another stall
				for(uint8_t i = 0 ; i < NUMBER_OF_STATIONS ; i++) {
					if (i != station && _last_tag[i] == tag) {
						_last_tag[i] = 0;
						debug_if(DBG, DBG_PFX "Calf moved from station %d to %d \r\n", (int)i, (int)station);
					}
				}
				//send the new tag to the meal manager
				_send_rfid(tag, station);
				_last_tag[station] = tag;
				debug_if(DBG, DBG_PFX "Sent tag %lu in %d \r\n", (uint32_t)tag, (int)station);
			}
		}
		else {
			//debug_if(DBG,DBG_PFX"Same calf %llu in station %llu \r\n",tag, (uint32_t)station);
		}
	}
	//If we didnt get a new tag check for timeout
	else if(_last_tag[station] != 0 && (time(NULL) - _tag_time[station]) > CALF_SESSION_TIMEOUT) {
		_send_rfid(0, station);
		_last_tag[station] = 0;
		for (int i = 0; i < RFID_READS; i++)
			_tag_history[station][i] = 0;
		debug_if(DBG, DBG_PFX "Timing out station %d \r\n", (int)station);
	}
}

void Cs_mf_rfid::rx_irq(Cs_mf_rfid *rfid) {
	//-- Get the active station
	uint8_t i = rfid->_active_station;
	//-- Make sure that there is a character to read
	if(rfid->_serial[i].readable()) {
		//-- Pull the character from the UART
		char cin = rfid->_serial[i].getc();
		if (cin == '\r') {
			rfid->_line_ready[i]++;
			//-- Replace the carriage return with a null character
			//-- This makes string handling easier
			cin = '\0';
		}
		//-- Put the new character in the buffer of the active station
		rfid->_buffer[i][rfid->_buffer_index[i]] = cin;
		rfid->_buffer_index[i]++;
	}
}

uint64_t Cs_mf_rfid::_extract_tag(uint8_t station) {
	//-- The buffer should have atleast the OK\r reponse from the SRA command
	//-- If a tag was received we will have received two carriage returns
	if(_line_ready[station] == 2) {
		int lenOK = strlen((const char *)_buffer[station]);
		char *buff = (char *)_buffer[station] + lenOK + 1;
		//-- We should have XXX_XXXXXXXXXXXX\0
		//-- EG             982_091002339170\0
		if(buff[3] == '_' && buff[16] == '\0') {
			//-- Move the first three numbers to remove the _
			buff[3] = buff[2];
			buff[2] = buff[1];
			buff[1] = buff[0];
			++buff;
			//-- Convert the tag
			//chirp(0.1);
			//Thread::wait(200);
			//chirp(0.1);
			debug_if(DBG, DBG_PFX "\aDetected Tag %s from %d \r\n", buff, (int)station);  //%s format specifier for a c string
			return strtoll(buff, NULL, 10);
		}
	}
	return 0;
}


void Cs_mf_rfid::_activate_rfid(uint8_t station) {
	_active_station = station;
	//-- Attach the ISR
    _serial[station].attach(callback(&rx_irq, this), RawSerial::RxIrq);
	//-- Clear the buffers
	std::fill(_buffer[station], _buffer[station] + sizeof(_buffer[station]), '\0');
	_buffer_index[station] = 0;
	_line_ready[station] = 0;
	//-- Send Set Reader Active
	_serial[station].puts("SRA\r");
}

void Cs_mf_rfid::_deactivate_rfid(uint8_t station) {
	//-- Detach the serial interrupt
	_serial[station].attach(NULL, RawSerial::RxIrq);
	//-- Send the Set Reader De-active command
	_serial[station].puts("SRD\r");
	Thread::wait(100);
	while (_serial[station].readable()) {
		_serial[station].getc();
	}
}


void Cs_mf_rfid::_send_rfid(uint64_t tag, uint8_t station) {
	//-- Send the tag to the meal manager
	calf_info_t *new_request = rfid_mailbox->alloc(100);
	if (new_request == NULL) {
		debug_if(DBG, DBG_PFX "Could not allocate mail\r\n");
	}
	else {
		debug_if(DBG, DBG_PFX " %llu\r\n", tag);
		//-- Populate the mail packet
		new_request->rfid = tag;
		new_request->station = station;
		new_request->timestamp = time(NULL);
		//-- Add the mail to the mail box
		rfid_mailbox->put(new_request);
	}
}

int8_t Cs_mf_rfid::_check_tag_history(uint8_t station) {
	//-- Check the history is all the same tag
	for(int i = 0 ; i < RFID_READS ; i++) {
		if (_tag_history[station][i] != _tag_history[station][0])
			return 0;
	}
	return 1;
}
