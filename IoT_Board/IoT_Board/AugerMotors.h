/*
 * cs_mf_manager.h
 *
 *  Created on: 7/08/2017
 *      Author: Tobin
 */

#ifndef CS_MF_MANAGER_H_
#define CS_MF_MANAGER_H_

#include <array>
#include "DigitalIn.h"
#include "DigitalOut.h"

class AugerMotors
{
public:
	enum AugerID
	{
		Stall1 = 0,
		Stall2 = 1,
		Stall3 = 2,
		Stall4 = 3,
	};
	
	AugerMotors(PinName hatchDetect, std::array<PinName, 5> augerPins);
	void setAugerDrive(AugerID motor, bool newState);
	bool readAugerState(AugerID motor);
	
	bool HatchOpen();
private:
	mbed::DigitalIn hatchOpen;
	std::array<mbed::DigitalOut, 5> augerMotors;
};

static_assert(AugerMotors::Stall1 == 0, "");

#endif 

