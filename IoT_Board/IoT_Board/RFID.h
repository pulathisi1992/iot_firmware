/*
 * cs_mf_rfid.h
 *
 *  Created on: 4/08/2017
 *      Author: Tobin
 */

#ifndef CS_MF_RFID_H_
#define CS_MF_RFID_H_

#include <mbed.h>
#include "Operations.h"
#include "Configurations.h"
#include "CommStructs.h"


class Cs_mf_rfid {
public:
	Cs_mf_rfid(RawSerial *serial, DigitalOut *enable);

	static void run(Cs_mf_rfid *rfid);
	static void rx_irq(Cs_mf_rfid *rfid);

	void setRfidMailbox(Mail<calf_info_t, 4>* rfidMailbox) {
		rfid_mailbox = rfidMailbox;
	}

private:
	RawSerial * _serial;
	DigitalOut * _enable;

	uint64_t _tag_history[NUMBER_OF_STATIONS][RFID_READS];
	uint64_t _last_tag[NUMBER_OF_STATIONS];
	uint8_t _history_index[NUMBER_OF_STATIONS];
	uint64_t _tag_time[NUMBER_OF_STATIONS];

	uint64_t _extract_tag(uint8_t station);
	void _process_station(uint8_t station);
	void _activate_rfid(uint8_t station);
	void _deactivate_rfid(uint8_t station);
	void _send_rfid(uint64_t tag, uint8_t station);
	int8_t _check_tag_history(uint8_t station);

	volatile char _buffer[NUMBER_OF_STATIONS][32];
	volatile uint8_t _line_ready[NUMBER_OF_STATIONS];
	volatile uint8_t _buffer_index[NUMBER_OF_STATIONS];
	volatile uint8_t _active_station;

	Mail<calf_info_t, 4> * rfid_mailbox;
};

#endif /* CS_MF_RFID_H_ */

