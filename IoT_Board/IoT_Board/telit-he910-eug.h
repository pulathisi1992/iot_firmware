/* telit-he910-eug.h
 *
 * Minimal implementation of HTTP requests using telit IP Easy commands
 *
 * Author Tobin Hall
 * email: tobin.g.hall@gmail.com
 */

#ifndef TELIT_HE910_EUG
#define TELIT_HE910_EUG

#include <chrono>
#include <cstdint>
#include <array>
#include <queue>
#include "rtos/Mutex.h"

#include "BufferedSerial.h"
#include "Array_Ref.h"
#include "ATParser.h"

enum class Modem_State
{
	Uninitialised = 0,
	  // clear the current state
	Initialised = 1,
	  // all settings set, URCs activated, and GPS activated
	Operator_Found = 2,
	  // survey completed
	Operator_Registered = 4, 
	 // registration completed
	Packet_Network_Attached = 8,
	  // modem attached
	PDP_Active = 16,
	  // PDP activated
	IP_Active = 32 // GPRS activated and IP obtained
};

// HE910-EUG has 5 valid PDP contexts (1 - 5)
enum class PDP_Context
{
	Context_1 = 1,
	Context_2 = 2,
	Context_3 = 3,
	Context_4 = 4,
	Context_5 = 5,
}
;

// HE910-EUG has 3 valid Http profiles (0 - 2)
enum class HTTP_Profile
{
	Profile_0 = 0,
	Profile_1 = 1,
	Profile_2 = 2,
}
;

class Telit {
public:
	enum class Result : int8_t {
		OK = 0,
		Timeout,
		Error,
		Reconnect // sent if one of the connections required to send data have been dropped
	};

	enum class Registration_State
	{
		Not_Registered = 0,
		Home_Network   = 1,
		Searching      = 2,
		Denied         = 3,
		Unknown        = 4,
		Roaming        = 5
	};

	Telit(PinName tx, PinName rx, PinName en, PinName pg);
	void reset();

	Result init();
	Result connect(PDP_Context context_id, String_Ref apn);

	Result setup_http(PDP_Context context_id, HTTP_Profile profile_id, String_Ref server_address, int server_port);
	Result http_post(HTTP_Profile profile_id, String_Ref url, Array_Ref<const uint8_t> data, String_Ref content_type, String_Ref extra_headers);
	Result wait_for_post_response(uint32_t& http_response, Array_Ref<uint8_t> response, Array_Ref<char> content_type);

	Result wait_for_network_reg(std::chrono::milliseconds timeout);

	int8_t get_rssi();

	struct Gps_Location
	{
		Result res;
		float latitude;
		float longitude;
	};
	Gps_Location get_location();
private:
	Result init_modem();
	Result init_gps();
	Result init_URC();

	Result network_survey();
	Result network_register(String_Ref operator_code);
	Result network_init_data_connection(PDP_Context context_ID, String_Ref apn);

	void Urc_CGREG_Handler();
	void Urc_CGEV_Handler();
	void Urc_CME_ERROR_Handler();

	// misc modem pins
	mbed::DigitalInOut _modem_enable;
	mbed::DigitalIn _modem_pg;
	// AT infrastructure
	BufferedSerial _serial;
	ATParser _parser;
	rtos::Mutex _at_mutex;
	// Identifiers
	std::array<char, 16> _IMEI {}
	; // 15 digit ID + '\0', init to all 0
	std::array<char, 21> _SIM_ID {}
	; // 20 digit ID + '\0', init to all 0
	std::array<char, 7> _Network_Numeric_ID {}
	; // 5 or 6 digit ID + '\0', init to all 0
	// modem state
	Modem_State _state = Modem_State::Uninitialised;
	std::array<bool, 5> _PDP_Activation_States = { false, false, false, false, false };  // 5 valid PDP contexts. Set the relavant index to true when active, false otherwise
	Registration_State _reg_state = Registration_State::Not_Registered;
	bool _GPRS_Attached = false;
	std::array<int, 4> _IP_Address {}
	;

	// network outage handling
	static constexpr std::chrono::minutes Base_Connection_Fail_Delay = std::chrono::minutes(30);
	static constexpr std::chrono::hours Max_Reconnect_Delay = std::chrono::hours(12);
	std::chrono::minutes _reconnect_delay = Base_Connection_Fail_Delay;
	std::chrono::system_clock::time_point _next_reconnect_time = std::chrono::system_clock::time_point::min();
};

#endif

