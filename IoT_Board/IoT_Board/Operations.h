/*
 * cs_mf_manager.h
 *
 *  Created on: 7/08/2017
 *      Author: Tobin
 */

#ifndef CS_MF_MANAGER_H_
#define CS_MF_MANAGER_H_

#include <rtos.h>
#include "Communications.h"
#include "Configurations.h"
#include "CommStructs.h"

class Cs_mf_manager {
public:
	Cs_mf_manager(DigitalOut *auger, DigitalIn *_auger_on, AnalogIn *meal_sensor, DigitalOut *meal_sensor_en, DigitalIn *safety_interlock, AnalogIn *motor_sense, DigitalOut *led_en);

	Mail<calf_info_t, FEED_MAILBOX_SIZE> rfid_mailbox;
	Mail<feed_info_t, LOG_MAILBOX_SIZE> feed_mailbox;

	static void run(Cs_mf_manager * manager);
	static void stop_auger(DigitalOut  *aug) {
		Cs_mf_manager::runningAugers--;
		aug->write(0);
	}
	static void stop_vibrator(DigitalOut  *vib) {
		if (Cs_mf_manager::runningAugers < 1)	vib->write(0);

	}
	void setLogMailbox(Mail<feed_info_t, LOG_MAILBOX_SIZE> * logMailbox) {
		log_mailbox = logMailbox;
	}
	void setRequestMailbox(Mail<calf_info_t, FEED_MAILBOX_SIZE> * requestMailbox) {
		request_mailbox = requestMailbox;
	}
	static int runningAugers;
	volatile uint8_t station_lockout_status;
	uint8_t enticement_mode = 0;
	uint8_t rfid_beep_enabled = 1;
	uint16_t enticement_time = 250;
	uint16_t sensor_calibration[4] = { 37000, 37000, 37000, 37000 };
	int duty[5] = { 0, 0, 0, 0, 0 };

private:
	int8_t process_mailboxes(void);
	int8_t process_station(uint8_t station);
	int8_t end_calf_session(uint8_t station);
	int8_t start_calf_session(uint8_t station);
	void dispense(uint8_t station);
	bool lockout(int station);
	bool meal_sensor_read(int station);
	int limit_current_pull(int killAll);
	bool channel_duty(int reads[], int channel);
	int amp_to_ADC_converter(int amps);


	Mail<calf_info_t, FEED_MAILBOX_SIZE> * request_mailbox;
	Mail<feed_info_t, LOG_MAILBOX_SIZE> * log_mailbox;

	DigitalOut * _auger_en;
	DigitalIn * auger_on;
	AnalogIn * motor_sense;
	DigitalOut * _meal_sensor_en;
	AnalogIn * _meal_sensor;
	DigitalIn * _safety_interlock;
	AnalogIn * _motor_sense;
	DigitalOut driver_cs_disable;
	DigitalOut * led_en;

	uint64_t sensor_reset_time;
	Timeout vibrtor_timeout;

	typedef struct {
		uint64_t current_tag;
		uint64_t last_tag_time;
		uint64_t last_dispense_time;

		uint16_t allocated_time;
		uint8_t allocated_units;
		uint8_t dispensed_units;

		uint8_t sensor_low_count;
		uint8_t sensor_high_count;
		uint8_t bowl_full;
		uint8_t has_augered;

		uint8_t dispense_fail_count;
		uint8_t lockout;
		uint8_t meal_detected_since_dispense;

		Timeout auger_timeout;
	}station_t;

	station_t stations[NUMBER_OF_STATIONS];


};

#endif /* CS_MF_MANAGER_H_ */

