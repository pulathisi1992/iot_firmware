/**
 * @file    BufferedSerial.cpp
 * @brief   Software Buffer - Extends mbed Serial functionallity adding irq driven TX and RX
 * @author  sam grove
 * @version 1.0
 * @see
 *
 * Copyright (c) 2013
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BufferedSerial.h"
#include <stdarg.h>
namespace
{
	char buffer[512];
	int BufferedPrintfC(BufferedSerial *stream, int size, const char* format, va_list arg)
	{
		if (size >= sizeof(buffer)) {
			return -1;
		}
		memset(buffer, 0, size);
		int r = vsnprintf(buffer, size - 1, format, arg);
		// this may not hit the heap but should alert the user anyways
		if(r > (int32_t) size) {
			error("%s %d buffer overwrite (max_buf_size: %d exceeded: %d)!\r\n", __FILE__, __LINE__, size, r);
			return 0;
		}
		if (r > 0) {
			stream->write(make_carray_ref(reinterpret_cast<const uint8_t *>(buffer), r));
		}
		return r;
	}
}

BufferedSerial::BufferedSerial(PinName tx, PinName rx, uint32_t buf_size, uint32_t tx_multiple, const char* name)
	: RawSerial(tx, rx)
	, _rxbuf(buf_size)
	, _txbuf(static_cast<uint32_t>(tx_multiple * buf_size))
	, _buf_size(buf_size)
	, _tx_multiple(tx_multiple)
	, _cbs()
{
	RawSerial::attach(callback(this, &BufferedSerial::rxIrq), Serial::RxIrq);
}

BufferedSerial::~BufferedSerial()
{
	RawSerial::attach(nullptr, RawSerial::RxIrq);
	RawSerial::attach(nullptr, RawSerial::TxIrq);
}

bool BufferedSerial::readable()
{
	return _rxbuf.available() > 0;  // note: look if things are in the buffer
}

bool BufferedSerial::writeable()
{
	return true;   // buffer allows overwriting by design, always true
}

char BufferedSerial::getc()
{
	return _rxbuf.get();
}

char BufferedSerial::putc(char c)
{
	_txbuf.put(c);
	BufferedSerial::prime();
	return c;
}

ssize_t BufferedSerial::puts(const char *s)
{
	if (s == nullptr) {
		return 0;
	}
	const char* ptr = s;
	while (*(ptr) != 0) {
		_txbuf.put(*(ptr++));
	}
	_txbuf.put('\n');   // done per puts definition
	BufferedSerial::prime();
	return (ptr - s) + 1;
}

ssize_t BufferedSerial::printf(const char* format, ...)
{
	va_list arg;
	va_start(arg, format);
	int r = BufferedPrintfC(this, this->_buf_size, format, arg);
	va_end(arg);
	return r;
}

ssize_t BufferedSerial::write(Array_Ref<const uint8_t> data_to_write)
{
	if (data_to_write.data() == nullptr || data_to_write.size() == 0) {
		return 0;
	}
	const char* ptr = reinterpret_cast<const char*>(data_to_write.begin());
	const char* end = ptr + data_to_write.size();

	while (ptr != end) {
		_txbuf.put(*(ptr++));
	}
	BufferedSerial::prime();

	return ptr - reinterpret_cast<const char*>(data_to_write.begin());
}


void BufferedSerial::rxIrq()
{
	// read from the peripheral and make sure something is available
	if(serial_readable(&_serial)) {
		_rxbuf.put(serial_getc(&_serial));  // if so load them into a buffer
		// trigger callback if necessary
		if(_cbs[RxIrq]) {
			_cbs[RxIrq]();
		}
	}
}

void BufferedSerial::txIrq()
{
	// see if there is room in the hardware fifo and if something is in the software fifo
	while(serial_writable(&_serial)) {
		if (_txbuf.available()) {
			serial_putc(&_serial, (int)_txbuf.get());
		}
		else {
			// disable the TX interrupt when there is nothing left to send
			RawSerial::attach(nullptr, RawSerial::TxIrq);
			// trigger callback if necessary
			if(_cbs[TxIrq]) {
				_cbs[TxIrq]();
			}
			break;
		}
	}
}

void BufferedSerial::prime()
{
	// if already busy then the irq will pick this up
	if(serial_writable(&_serial)) {
		RawSerial::attach(nullptr, RawSerial::TxIrq);     // make sure not to cause contention in the irq
		BufferedSerial::txIrq();                 // only write to hardware in one place
		RawSerial::attach(callback(this, &BufferedSerial::txIrq), RawSerial::TxIrq);
	}
}

void BufferedSerial::attach(Callback<void()> func, IrqType type)
{
	_cbs[type] = func;
}

