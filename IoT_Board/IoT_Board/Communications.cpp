/*
 * cs_mf_com.cpp
 *
 *  Created on: 27/07/2017
 *      Author: Tobin
 */


#include <Communications.h>
#include <rtos.h>
#include "Configurations.h"
#include "CommStructs.h"
#include "PinAlloc.h"

namespace
{
	constexpr bool DBG = true;
#define DBG_PFX "\e[1;36mCOMS: "

	constexpr int Modem_Failure = -1;
	constexpr int Modem_Reconnect = -2;
}

constexpr char Cs_mf_com::octet_stream[];

Cs_mf_com::Cs_mf_com()
	: _modem(MODEM_TX, MODEM_RX, MODEM_EN, MODEM_PG)
	, _response_content_type()
	, _asset_id()
	, _ready(0)
	, _state(Com_States::Init)
	, _failcount(0)
	, _previous_operating_mode(0)
	, first_status_timeout()
	, feed_mailbox(nullptr)
{
	//copy the microcontroller uid to the assetid string
    uint32_t * uid = (uint32_t *)UID_BASE;
	sprintf(_asset_id, "\"asset_id:%08lX%08lX%08lX\"", uid[0], uid[1], uid[2]);
	debug_if(DBG, "%s\r\n", _asset_id);
}

void Cs_mf_com::run(Cs_mf_com * com) {
	//debug_if(DBG,DBG_PFX"Starting\r\n");
    uint32_t * uid = (uint32_t *)UID_BASE;
	debug_if(DBG, DBG_PFX " %s %d %s\r\n", Host_Name.data(), Port_Number, Status_Url.data());
	while (1) {
		switch (com->_state) {
		case Com_States::Init:
			if (com->init() == 1) {
				com->_state = Com_States::Waiting_For_Request;
			}
			else {
				com->_state = Com_States::Reset;
			}
			break;
		case Com_States::Reset:
			com->_ready = 0;
			com->statistics.reboot_count++;
			com->_modem.reset();
			com->_state = Com_States::Init;
			break;
		case Com_States::Waiting_For_Request:
			com->process_mailboxes();
			if (com->_failcount > REBOOT_FAIL_COUNT) {
				com->_state = Com_States::Reset;
			}
			break;
		case Cs_mf_com::Com_States::Busy:
			break;
		}
		//Thread::wait(100);
		wait(100);
	}
}

int8_t Cs_mf_com::get_location() {
	return 1;
}

int8_t Cs_mf_com::process_mailboxes(void) {
	osEvent evt = request_mailbox.get(0);
	if (evt.status == osEventMail) {
		calf_info_t *new_request = (calf_info_t *)evt.value.p;

		debug_if(DBG, DBG_PFX "Got a request from %u for %lu \r\n", (uint32_t)new_request->station, (uint32_t)new_request->rfid);
		_ready = 0;
		int16_t res = send_feed_request(new_request);
		//-- We will get a -1 for a modem failure
		if(res == Modem_Failure) {
			_failcount++;
			//-- Put the mail back onto the queue
			request_mailbox.put(new_request);
		}//-- We will get a 400 or 500 on a bad response
		else if(res == Modem_Reconnect) {
			 // -2 if the modem needs to reconnect
		    _state = Com_States::Init;
			//-- Put the mail back onto the queue
			request_mailbox.put(new_request);
		}
		else if(res == 500 && (time(nullptr) - new_request->timestamp) > COM_PACKET_TIMEOUT) {
			//-- Put the mail back onto the queue
			request_mailbox.put(new_request);
			statistics.lost_packet_count++;
		}
		else {
			request_mailbox.free(new_request);
		}
		_ready = 1;
	}
	evt = log_mailbox.get(0);
	if (evt.status == osEventMail) {
		feed_info_t *new_request = (feed_info_t *)evt.value.p;

		_ready = 0;
		int16_t res = send_feed_log(new_request);
		//-- We will get a -1 for a modem failure
		if(res == Modem_Failure) {
			_failcount++;
			//-- Put the mail back onto the queue
			log_mailbox.put(new_request);
		}//-- We will get a 400 or 500 on a bad response
		else if(res == Modem_Reconnect) {
			 // -2 if the modem needs to reconnect
		    _state = Com_States::Init;
			//-- Put the mail back onto the queue
			log_mailbox.put(new_request);
		}
		else if(res == 500 && (time(nullptr) - new_request->timestamp) > COM_PACKET_TIMEOUT) {
			//-- Put the mail back onto the queue
			log_mailbox.put(new_request);
			statistics.lost_packet_count++;
		}
		else {
			log_mailbox.free(new_request);
		}
		_ready = 1;
	}
	evt = status_mailbox.get(0);
	if (evt.status == osEventMail) {
		status_t *new_request = (status_t *)evt.value.p;
		//debug_if(DBG,DBG_PFX"Got a status request\r\n");
		_ready = 0;
		int16_t res = send_status(new_request);
		//-- We will get a -1 for a modem failure
		if(res == Modem_Failure) {
			_failcount++;
			//-- Put the mail back onto the queue
			status_mailbox.put(new_request);
		}//-- We will get a 400 or 500 on a bad response
		else if(res == Modem_Reconnect) {
			 // -2 if the modem needs to reconnect
		    _state = Com_States::Init;
			//-- Put the mail back onto the queue
			status_mailbox.put(new_request);
		}
		else if(res == 500 && (time(nullptr) - new_request->timestamp) > COM_PACKET_TIMEOUT) {
			//-- Put the mail back onto the queue
			status_mailbox.put(new_request);
			statistics.lost_packet_count++;
		}
		else {
			status_mailbox.free(new_request);
		}
		_ready = 1;
	}
	return 1;
}

int16_t Cs_mf_com::send_feed_log(feed_info_t * log_request) {

	Timer tick;
	tick.start();
	log_packet.rfid = log_request->rfid;
	log_packet.station = log_request->station;
	log_packet.timestamp = log_request->timestamp;
	log_packet.auger_time = log_request->auger_time;
	log_packet.auger_units = log_request->auger_units;

	debug_if(DBG, DBG_PFX "logging %u units of %u ms for %lu\r\n", log_request->auger_units, log_request->auger_time, (uint32_t)log_request->rfid);
	Telit::Result res = _modem.http_post(HTTP_Profile::Profile_0,
		Log_Url,
		make_carray_ref(reinterpret_cast<const uint8_t *>(&log_packet), sizeof(log_packet)),
		octet_stream,
		_asset_id);
	if (res == Telit::Result::Reconnect) {
		return -2;
	}
	if (res != Telit::Result::OK) {
		return -1;
	}
	uint32_t http_status;
	Telit::Result res_code = _modem.wait_for_post_response(http_status,
		Array_Ref<uint8_t>(nullptr, 0),
		Array_Ref<char>((char *)&_response_content_type, sizeof(_response_content_type)));
	if (http_status != 200) {
		return http_status;
	}
	if (res_code != Telit::Result::OK) {
		return -1;
	}
	statistics.average_time = (statistics.average_time * TIME_FF + tick.read_ms()) / (TIME_FF + 1);
	_failcount = 0;
	return 1;
}

int16_t Cs_mf_com::send_feed_request(calf_info_t* feed_request) {
	Timer tick;
	tick.start();
	request_packet.rfid = feed_request->rfid;
	request_packet.station = feed_request->station;
	request_packet.timestamp = feed_request->timestamp;

	Telit::Result res = _modem.http_post(HTTP_Profile::Profile_0,
		Feed_Url,
		make_carray_ref(reinterpret_cast<const uint8_t *>(&request_packet), sizeof(request_packet)),
		octet_stream,
		_asset_id);
	if (res == Telit::Result::Reconnect) {
		return Modem_Reconnect;
	}
	if (res != Telit::Result::OK) {
		return Modem_Failure;
	}
	uint32_t http_status;
	Telit::Result res_code = _modem.wait_for_post_response(http_status,
		Array_Ref<uint8_t>((uint8_t *)&response_packet, sizeof(response_packet)),
		Array_Ref<char>(reinterpret_cast<char*>(&_response_content_type), sizeof(_response_content_type)));
	if (http_status != 200) {
		return http_status;
	}
	if (res_code != Telit::Result::OK) {
		return Modem_Failure;
	}
	statistics.average_time = (statistics.average_time * TIME_FF + tick.read_ms()) / (TIME_FF + 1);
	feed_info_t * feed_response = feed_mailbox->alloc(100);
	if (feed_response == nullptr) {
	}
	else {
		debug_if(DBG, "Feed request reply  auger time: %u,  units: %u\r\n", (uint32_t)response_packet.auger_time, (uint32_t)response_packet.auger_units);

		feed_response->rfid = feed_request->rfid;
		feed_response->station = feed_request->station;
		feed_response->auger_time = response_packet.auger_time;
		feed_response->auger_units = response_packet.auger_units;
		feed_response->timestamp = time(nullptr);
		feed_mailbox->put(feed_response);
	}
	_failcount = 0;
	if (_previous_operating_mode & CTRL_RFID_BEEP) {
		chirp(0.1);
	}
	return 1;
}

int16_t Cs_mf_com::send_status(status_t * status_mail) {

	//-- Fill in the packet from the mail
    status_packet.uptime = status_mail->uptime;
	status_packet.battery_level = status_mail->battery_level;
	status_packet.hopper_level = status_mail->hopper_level;
	status_packet.solar_panel_power = status_mail->solar_panel_power;
	status_packet.solar_consumed_energy = status_mail->solar_consumed_energy;
	status_packet.solar_generated_energy = status_mail->solar_generated_energy;
	status_packet.station_lock_status = status_mail->lockout;
	status_packet.operating_mode = (status_mail->hatch_open << 1) | (_previous_operating_mode & CTRL_ENTICEMENT);

	//-- Fill in the packet with content from the com class:
status_packet.com_error_count = statistics.lost_packet_count;
	status_packet.com_reboot_count = statistics.reboot_count;
	status_packet.rssi = _modem.get_rssi();
	status_packet.average_transaction_time = statistics.average_time;
	//-- The GPS data is yet to be implemented
	status_packet.longitude = 0.0;
	status_packet.latitude = 0.0;

	Telit::Gps_Location gps_result = _modem.get_location();
	if (gps_result.res == Telit::Result::OK) {
		status_packet.longitude = gps_result.longitude;
		status_packet.latitude = gps_result.latitude;
	}

	Telit::Result res =  _modem.http_post(HTTP_Profile::Profile_0,
		Status_Url,
		make_carray_ref(reinterpret_cast<const uint8_t*>(&status_packet), sizeof(status_packet)),
		octet_stream,
		_asset_id);
	if (res == Telit::Result::Reconnect) {
		return Modem_Reconnect;
	}
	if (res != Telit::Result::OK) {
		return Modem_Failure;
	}
	uint32_t http_status;
	Telit::Result res_code = _modem.wait_for_post_response(http_status,
		make_array_ref(reinterpret_cast<uint8_t *>(&status_response_packet), sizeof(status_response_packet)),
		make_array_ref(_response_content_type, sizeof(_response_content_type)));
	if (http_status != 200) {
		return http_status;
	}
	if (res_code != Telit::Result::OK) {
		return Modem_Failure;
	}
	debug_if(DBG,
		DBG_PFX"Got status packet, enticement mode:%u, adlib time:%u, rfid beep enabled: %u\r\n",
		(uint32_t)status_response_packet.control & CTRL_ENTICEMENT,
		(uint32_t)status_response_packet.adlib_auger_time,
		(uint32_t)status_response_packet.control & CTRL_RFID_BEEP);
	//-- Check if a reset has been sent
if(status_response_packet.control & CTRL_RESTART) {
		debug_if(DBG, DBG_PFX "Received Restart Command From Server!!");
		// In order to work correctly there must be a low level on boot0 (PF_11)
		// otherwise the system will lockup. We could use a jumper on the board.
		DigitalOut boot0(PF_11);
		boot0.write(0);
		wait_us(100);
		NVIC_SystemReset();
	}

	//led_en[1].write(1);
	//led_timeout.attach(callback(stop_led, &led_en[1]), 540 );

		//-- Check if the mode has changed
		//if((status_response_packet.control & (CTRL_ENTICEMENT | CTRL_RFID_BEEP)) != previous_operating_mode){
			//Send the mode to the meal manager, this is done by rfid = 0, station = 0xFF
feed_info_t * feed_response = feed_mailbox->alloc(0);
	if (feed_response == nullptr) {
		//debug_if(DBG,DBG_PFX"Could not allocate mail\r\n");
	}
	else {
		feed_response->rfid = 0;
		feed_response->station = 0xFF;
		feed_response->auger_time = status_response_packet.adlib_auger_time;
		feed_response->auger_units = status_response_packet.control & (CTRL_ENTICEMENT | CTRL_RFID_BEEP);
		feed_response->timestamp = time(nullptr);
		feed_response->timestamp = time(nullptr);

		feed_response->sensor_calibration[0] = 	status_response_packet.sensor0;
		feed_response->sensor_calibration[1] = 	status_response_packet.sensor1;
		feed_response->sensor_calibration[2] = 	status_response_packet.sensor2;
		feed_response->sensor_calibration[3] = 	status_response_packet.sensor3;

		debug_if(DBG, DBG_PFX"Status response:");
		debug_if(DBG, "auger_time: %u \r\n", (uint32_t)feed_response->auger_time);
		debug_if(DBG, "control byte: %u \r\n", (uint32_t)feed_response->auger_units);

		feed_mailbox->put(feed_response);
		_previous_operating_mode = status_response_packet.control & (CTRL_ENTICEMENT | CTRL_RFID_BEEP);
	}
	//}

		/*feed_info_t * status_delay = status_delay_mailbox->alloc(0);
		status_delay->status_delay = status_response_packet.status_delay;
		if(status_delay_mailbox == nullptr){
			//
		} else {
			// TODO
		}*/

		return 1;
}

int8_t Cs_mf_com::init() {
	if (_modem.init() != Telit::Result::OK) {
		return Modem_Failure;
	}
	Telit::Result res = _modem.connect(PDP_Context::Context_1, "spe.inetd.gdsp");  // "internet" - 2degrees
	if(res != Telit::Result::OK) {
		return Modem_Failure;
	}
	res = _modem.setup_http(PDP_Context::Context_1, HTTP_Profile::Profile_0, Host_Name, Port_Number);
	if (res != Telit::Result::OK) {
		return Modem_Failure;
	}
	//-- connected
	_ready = 1;
	_failcount = 0;
	chirp(2.0);

	return 1;
}



