/*
 * cs_mf_com.h
 *
 *  Created on: 27/07/2017
 *      Author: Tobin
 */

#ifndef CS_MF_COM_H_
#define CS_MF_COM_H_

#include "telit-he910-eug.h"
#include "mbed.h"
#include "rtos.h"
#include "Configurations.h"
#include "CommStructs.h"

class Cs_mf_com {
public:
	Cs_mf_com();

	struct com_stats {
		uint16_t average_time;
		uint8_t lost_packet_count;
		uint8_t reboot_count;
		float longitude;
		float latitude;
	}statistics;

	struct __attribute__((__packed__)) meal_feed_request {
		uint64_t rfid;
		uint8_t station;
		uint64_t timestamp;
	}request_packet;

	struct __attribute__((__packed__)) meal_feed_log {
		uint64_t rfid;
		uint8_t station;
		uint64_t timestamp;
		uint16_t auger_time;
		uint8_t auger_units;
	}log_packet;

	struct __attribute__((__packed__)) status {
		uint64_t uptime;
		float32_t longitude;
		float32_t latitude;
		uint32_t solar_panel_power;
		uint32_t solar_generated_energy;
		uint32_t solar_consumed_energy;
		uint16_t solar_controller_temp;
		uint16_t battery_level;
		uint16_t average_transaction_time;
		uint8_t hopper_level;
		int8_t rssi;
		uint8_t com_error_count;
		uint8_t com_reboot_count;
		uint8_t station_lock_status;
		uint8_t operating_mode;
	}status_packet;

	struct __attribute__((__packed__)) meal_feed_response {
		uint16_t auger_time;
		uint8_t auger_units;
	}response_packet;

	struct __attribute__((__packed__)) status_response {
		uint16_t adlib_auger_time;
		uint8_t control;
		uint16_t status_delay;
		uint16_t sensor0;
		uint16_t sensor1;
		uint16_t sensor2;
		uint16_t sensor3;
	}status_response_packet;

	Mail<calf_info_t, FEED_MAILBOX_SIZE> request_mailbox;
	Mail<feed_info_t, LOG_MAILBOX_SIZE> log_mailbox;
	Mail<status_t, STATUS_MAILBOX_SIZE> status_mailbox;


	void setFeedMailbox(Mail<feed_info_t, 4>* feedMailbox) {
		feed_mailbox = feedMailbox;
	}

	static void run(Cs_mf_com * com);
	static void stop_led(DigitalOut  *led) {
		led->write(0);
	}
private:
	int8_t init();

	int16_t send_feed_log(feed_info_t * log_request);
	int16_t send_feed_request(calf_info_t * feed_request);
	int16_t send_status(status_t * status_mail);
	int8_t get_location();
	int8_t process_mailboxes();


	enum class Com_States
	{
		Init                = 0,
		Busy,
		Waiting_For_Request,
		Reset
	};

	enum class Mail_t
	{
		Feed   = 0,
		Log,
		Status
	};

	Telit _modem;
	char _response_content_type[32];
	char _asset_id[38];
	int8_t _ready;
	Com_States _state = Com_States::Init;
	uint8_t _failcount;
	uint8_t _previous_operating_mode;
	Timeout first_status_timeout;

	Mail<feed_info_t, 4> * feed_mailbox = nullptr;

	static constexpr char octet_stream[] { "application/octet-stream" }
	;

};

#endif /* CS_MF_COM_H_ */

