/* telit-he910-eug.h
 *
 * Minimal implementation of HTTP requests using telit IP Easy commands
 *
 * Author Tobin Hall
 * email: tobin.g.hall@gmail.com
 * xxxx
 */

#include "telit-he910-eug.h"
#include <chrono>
#include <algorithm>
#include <rtos.h>
#include <mbed.h>
#include <mutex>
#include "Auto_Timer.h"

namespace
{
	std::array<String_Ref, 3> Network_Codes
	{
		"53001",  // Vodafone
		"53005",  // Spark
		"53024"  // 2degrees
	}
	;

	constexpr bool Debug_Print = true;
	void thread_wait(std::chrono::nanoseconds delay)
	{
		using namespace std::chrono;
		Thread::wait(duration_cast<std::chrono::milliseconds>(delay).count());
	}

	constexpr int Modem_Buffer_Size = 1024;
	constexpr std::chrono::seconds Http_Timeout(10);
	static char tmp_buff[200];

	Modem_State operator~(Modem_State lhs)
	{
		return static_cast<Modem_State>(~static_cast<int>(lhs));
	}

	Modem_State operator|(Modem_State lhs, Modem_State rhs)
	{
		return static_cast<Modem_State>(static_cast<int>(lhs) | static_cast<int>(rhs));
	}

	void operator|=(Modem_State& lhs, Modem_State rhs)
	{
		lhs = lhs | rhs;
	}

	Modem_State operator&(Modem_State lhs, Modem_State rhs)
	{
		return static_cast<Modem_State>(static_cast<int>(lhs) & static_cast<int>(rhs));
	}

	void operator&=(Modem_State& lhs, Modem_State rhs)
	{
		lhs = lhs & rhs;
	}

	bool state_test(Modem_State& current, Modem_State against)
	{
		return against == (current & against);
	}

	void state_set(Modem_State& current, Modem_State to_set)
	{
		current |= to_set;
	}

	void state_reset(Modem_State& current, Modem_State to_reset)
	{
		current &= ~to_reset;
	}

	Telit::Result modem_attach(ATParser& parser)
	{
		int attach_state;
		if (!(parser.send("AT+CGATT?")
		      && parser.recv("+CGATT: %d", &attach_state)
		      && parser.recv("OK"))) // expected example (+CGATT: 0)
			{
				return Telit::Result::Timeout; // "no response to attach query"
			}
		if (attach_state != 1) {
			 // if not attached
		    // 10 second timeout required by Vodafone
		    parser.setTimeout(std::chrono::seconds(60));
			if (!(parser.send("AT+CGATT=1")
			      && parser.recv("OK")))
			{
				 // responds with \r\nOK\r\n
			    return Telit::Result::Timeout;
			}
		}
		return Telit::Result::OK;
	}

	Telit::Result activate_PDP(ATParser& parser, PDP_Context context, String_Ref apn)
	{
		parser.send("AT+CGACT?");
		snprintf(tmp_buff, sizeof(tmp_buff) - 1, "+CGACT: %d,1", static_cast<int>(context));
		if (!(parser.recv(tmp_buff) // if this context isn't active
		      && parser.recv("OK")))
		{
			// responds with (+CGACT: <id>,<state>\r\n) for each defined context
			parser.send("AT+CGDCONT=%d,IP,%s", context, apn.data());
			if (!parser.recv("OK"))
			{
				return Telit::Result::Timeout;
			}
			// 45 second timeout required by Vodafone
			parser.setTimeout(std::chrono::seconds(60));
			parser.send("AT+CGACT=%d,1", context);
			if (!parser.recv("OK")) {
				return Telit::Result::Timeout;
			}
		}
		return Telit::Result::OK;
	}

	Telit::Result activate_gprs(ATParser& parser, bool& current_gprs_state, std::array<int, 4>& IP)
	{
		parser.setTimeout(std::chrono::seconds(15));
		parser.send("AT#GPRS?");
		int gprs_state;
		if (!(parser.recv("#GPRS: %d", &gprs_state)
		      && parser.recv("OK")))
		{
			return Telit::Result::Timeout;
		}
		current_gprs_state = (1 == gprs_state);
		if (!current_gprs_state) {
			parser.setTimeout(std::chrono::seconds(15));
			parser.send("AT#GPRS=1");
			if (!(parser.recv("+IP: %d.%d.%d.%d%*[\r]", IP.data(), IP.data() + 1, IP.data() + 2, IP.data() + 3)
			      && parser.recv("OK")))
			{
				return Telit::Result::Timeout;

			}
			current_gprs_state = true;
		}
		return Telit::Result::OK;
	}

	constexpr bool registered(Telit::Registration_State reg)
	{
		return reg == Telit::Registration_State::Home_Network || reg == Telit::Registration_State::Roaming;
	}
}

Telit::Telit(PinName tx, PinName rx, PinName en, PinName pg)
    : _modem_enable(en)
    , _modem_pg(pg)
    , _IMEI()
    , _serial(tx, rx, Modem_Buffer_Size)
    , _parser(_serial, "\r\n", "\r") // \r ends a command in both terse and verbose response modes. Verbose also includes a '\n' following this
{
	_serial.baud(115200);

	_parser.debugOn(true);
	_parser.oob("+CGREG: ", callback(this, &Telit::Urc_CGREG_Handler));
	_parser.oob("+CGEV: ", callback(this, &Telit::Urc_CGEV_Handler));
	_parser.oob("+CME ERROR: ", callback(this, &Telit::Urc_CME_ERROR_Handler));
	// so far have been unable to trigger +CIEV: URC. Querying with AT+CIND? is clearer
	// _parser.oob("+CIEV: ", callback(this, &Telit::Urc_CIEV_Handler));

	_modem_enable.mode(OpenDrainNoPull);
	_modem_enable.output();
	_modem_enable.write(true);
}

Telit::Result Telit::init() {
	Auto_Timer tick;
	_modem_enable.write(true);
	thread_wait(std::chrono::milliseconds(2500));
	tick.reset();

	{
		std::lock_guard<rtos::Mutex> lk(_at_mutex);
		_parser.setTimeout(std::chrono::milliseconds(500));
		while (true) // AT\r\n\r\nOK\r\n
			{
				_parser.send("ATV1");
				if (_parser.recv("OK")) {
					break;
				}
				if (tick.read() > std::chrono::seconds(5)) {
					return Result::Timeout;
				}
			}
	}
	debug_if(Debug_Print, "init time %dms\r\n", static_cast<int>(std::chrono::duration_cast<std::chrono::milliseconds>(tick.read()).count()));
	tick.reset();
	if (!state_test(_state, Modem_State::Initialised)) {
		// init modem settings
		if(init_modem() != Result::OK) {
			return Result::Timeout;
		}
		// initialise URCs
		if(init_URC() != Result::OK) {
			return Result::Timeout;
		}
		// init GPS
		if(init_gps() != Result::OK) {
			return Result::Timeout;
		}
		debug_if(Debug_Print, "command time %d\r\n", static_cast<int>(std::chrono::duration_cast<std::chrono::milliseconds>(tick.read()).count()));
		state_set(_state, Modem_State::Initialised);
	}

	return Result::OK;
}

Telit::Result Telit::init_modem()
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	// enable command echo
	_parser.send("ATE1");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// Serial - disable flow control
	_parser.send("AT&K0");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// serial baud rate = 115200
	_parser.send("AT+IPR=115200");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// serial framing = 8N1
	_parser.send("AT+ICF=3");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// set profile to #0
	_parser.send("AT&P0");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// network reported using numeric code
	_parser.send("AT+COPS=3,2");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// sim detection through SIMIN pin
	_parser.send("AT#SIMDET=2");
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// read the modem IMEI (product serial number ID).
	// Note that there is no read (?) or execution (=) version of this command
	_parser.send("AT#CGSN");
	if (!(_parser.recv("#CGSN: %15[^\r]%*[\r]", _IMEI.data()) // %s is not greedy, so must specify as %[^\r]%[\r] (any character until '\r')
	      && _parser.recv("OK")))
	{
		return Result::Timeout;
	}
	debug_if(Debug_Print, "IMEI: %s\r\n", _IMEI.data());
	// read the SIM ID code
	// Note that there is no read (?) or execution (=) version of this command
	_parser.send("AT#CCID");
	if (!(_parser.recv("#CCID: %20[^\r]%*[\r]", _SIM_ID.data()) // matching up to 20 characters ending at '\r'
	      && _parser.recv("OK")))
	{
		return Result::Timeout;
	}
	debug_if(Debug_Print, "%s\r\n", _SIM_ID.data());
	// set power mode to never go to sleep (require CTS/RTS enabled to take advantage of power saving modes)
	_parser.send("AT+CFUN=1");
	if (!_parser.recv("OK"))
	{
		return Result::Timeout;
	}
	// write the current state as profile #0
	_parser.send("AT&W0");
	if (!_parser.recv("OK"))
	{
		return Result::Timeout;
	}

	thread_wait(std::chrono::milliseconds(1000));
	return Result::OK;
}

Telit::Result Telit::init_URC()
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(500));
	// Network event reporting
	// +CGEV...
	if(!(_parser.send("AT+CGEREP=2") && _parser.recv("OK")))
	{
		return Result::Timeout;
	}
	// Mobile Equipment (ME) error reporting in numeric format
	// +CME ERROR...
	if(!(_parser.send("AT+CMEE=2") && _parser.recv("OK")))
	{
		return Result::Timeout;
	}
	// Network registration status
	// +CGREG: <0-5>
	if(!(_parser.send("AT+CGREG=2") && _parser.recv("OK")))
	{
		return Result::Timeout;
	}
	thread_wait(std::chrono::milliseconds(1000));
	return Result::OK;
}

Telit::Result Telit::network_survey()
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::seconds(1));
	//_parser.send("AT#CSURVF=1");
	//_parser.recv("OK");
	//
	    //_parser.setTimeout(std::chrono::seconds(100));
	    //_parser.send("AT+CSURV");

	    strncpy(_Network_Numeric_ID.data(), "53001", _Network_Numeric_ID.size());  // hardcoding preferred operator for now
	    thread_wait(std::chrono::milliseconds(1000));
	return Result::OK;
}

Telit::Result Telit::network_register(String_Ref operator_code)
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::seconds(2));
	// update operator state
	std::array<char, 8> registered_operator {}
	;
	_parser.send("AT+COPS?");
	_parser.recv("+COPS: %*d,2,\"%6[^\"]\",%*d", registered_operator.data());
	_parser.recv("OK");
	// update registration state
	if(operator_code != String_Ref(registered_operator.data(), strlen(registered_operator.data()))) {
		// make sure we're diconnected before making a connection
		_parser.send("AT+CGATT=0");
		_parser.recv("OK");
		_parser.send("AT+COPS=2,2");
		_parser.recv("OK");
		thread_wait(std::chrono::milliseconds(1000));
		Timer t;
		t.start();
		while (t.read_ms() < std::chrono::milliseconds(5000).count()) {
			if (_GPRS_Attached || std::any_of(_PDP_Activation_States.begin(), _PDP_Activation_States.end(), [](bool st) { return st == true; })) {
				continue;
			}
			break;
		}
	}
	// update registration status
	_parser.send("AT+CGREG?");
	_parser.recv("OK");
	// are we already registered to the operator
	if(registered(_reg_state)) {
		debug_if(Debug_Print, "already registered with operator %6s\r\n", _Network_Numeric_ID.data());
		state_set(_state, Modem_State::Operator_Found);
		return Result::OK;
	}
	// now register
	_parser.send("AT+COPS=4,2,%s", operator_code.data());
	if (!_parser.recv("OK")) {
		return Result::Timeout;
	}
	// wait for registration
	Auto_Timer t;
	while (t.read() < std::chrono::seconds(45)) {
		_parser.send("AT+CGREG?");  // noisy and not required, but much easier to debug
		if(!_parser.recv("OK")) {
			continue;
		}
		if (_reg_state == Registration_State::Home_Network || _reg_state == Registration_State::Roaming) {
			_parser.send("AT+COPS?");
			_parser.recv("+COPS: %*d,2,\"%6[^\"]\",%*d", _Network_Numeric_ID.data());  // who did we actually connect to
			state_set(_state, Modem_State::Operator_Found);
			return Result::OK;
		}
		thread_wait(std::chrono::milliseconds(1000));
	}
	return Result::Timeout;
}

Telit::Result Telit::network_init_data_connection(PDP_Context context_ID, String_Ref apn)
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::seconds(1));
	Result res = modem_attach(_parser);
	if (res != Telit::Result::OK) {
		return res;
	}
	state_set(_state, Modem_State::Packet_Network_Attached);

	res = activate_PDP(_parser, context_ID, apn);
	if (res != Telit::Result::OK) {
		return res;
	}
	_PDP_Activation_States[static_cast<int>(context_ID) - 1] = true;
	state_set(_state, Modem_State::PDP_Active);

	// GPRS gets the modem an IP address, is required == 1 prior to any http transmissions
	res = activate_gprs(_parser, _GPRS_Attached, _IP_Address);
	if (res != Telit::Result::OK) {
		return res;
	}
	state_set(_state, Modem_State::IP_Active);
	return Result::OK;
}

Telit::Result Telit::connect(PDP_Context context_id, String_Ref apn)
{
	while (_next_reconnect_time > std::chrono::system_clock::from_time_t(time(nullptr)) && !registered(_reg_state)) {
		using namespace std::chrono;
		debug_if(Debug_Print,
			"reconnect attempt at %d, remaining %ds\r\n",
			system_clock::to_time_t(_next_reconnect_time),
			static_cast<int>(duration_cast<seconds>(_next_reconnect_time - system_clock::from_time_t(time(nullptr))).count()));
		thread_wait(std::chrono::minutes(1));
		_parser.process_oob();
	}
	auto survey_result = network_survey();
	if (survey_result == Result::OK) {
		constexpr int Max_Retries = 3;
		for (int i = 0; i < Max_Retries; ++i) {
			if (network_register(String_Ref(_Network_Numeric_ID.data(), strlen(_Network_Numeric_ID.data()))) == Result::OK) {
				for (int i = 0; i < Max_Retries; ++i) {
					if (network_init_data_connection(context_id, apn) == Result::OK) {
						_reconnect_delay = Base_Connection_Fail_Delay;
						return Result::OK;
					}
				}
				break;
			}
		}
	}
	// network connection backoff
	_next_reconnect_time = std::chrono::system_clock::from_time_t(time(nullptr)) + _reconnect_delay;
	// double delay on each fail
	// cap reconnect_delay at 12 hours
	//_reconnect_delay = std::min<std::chrono::minutes>(_reconnect_delay * 2, Max_Reconnect_Delay);
	using namespace std::chrono;
	debug_if(Debug_Print,
		"failed to connect, reconnect attempt at %d, remaining %ds\r\n",
		std::chrono::system_clock::to_time_t(_next_reconnect_time),
		static_cast<int>(duration_cast<seconds>(_next_reconnect_time - system_clock::from_time_t(time(nullptr))).count()));
	return Result::Timeout;
}

Telit::Result Telit::setup_http(PDP_Context context_id, HTTP_Profile profile_id, String_Ref server_address, int server_port)
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	_parser.send("AT#HTTPCFG=%d,%s,%d,0,,,0,%ld,%d",
		static_cast<int>(profile_id),
		server_address.data(),
		server_port,
		static_cast<std::uint32_t>(Http_Timeout.count()),
		static_cast<int>(context_id));
	if (_parser.recv("OK")) {
		return Result::OK;
	}
	return Result::Timeout;
}

Telit::Result Telit::http_post(HTTP_Profile profile_id,
	String_Ref url,
	Array_Ref<const uint8_t> data,
	String_Ref content_type,
	String_Ref extra_headers)
{
	_parser.process_oob();
	if (_reg_state != Registration_State::Home_Network
	    && _reg_state != Telit::Registration_State::Roaming)
	{
		return Result::Reconnect;
	}
	if (!_PDP_Activation_States[0]) {
		 // TODO: http cfg needs to set the pdp its using for this check
	    return Result::Reconnect;
	}
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::seconds(1));
	_parser.send("AT#GPRS?");
	if (!(_parser.recv("#GPRS: 1")
	      && _parser.recv("OK")))
	{
		return Result::Reconnect;
	}
	_parser.setTimeout(std::chrono::seconds(60));
	//AT#HTTPSND=<prof_id>,<command>,<resource>,<data_len>,<post_param>,<extra_header_line>
	_parser.send("AT#HTTPSND=%u,0,%s,%u,%s,%s",
		static_cast<int>(profile_id),
		url.data(),
		data.size(),
		content_type.data(),
		extra_headers.data());
	if (_parser.recv(">>> "))
	{
		_parser.write(data);
		if (_parser.recv("OK")) {
			return Result::OK;
		}
	}
	return Result::Error;
}

Telit::Result Telit::wait_for_post_response(uint32_t& http_response,
	Array_Ref<uint8_t> response,
	Array_Ref<char> content_type)
{
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(Http_Timeout));
	int profile_id, response_length;
	if (!_parser.recv("#HTTPRING: %u,%u,%[^,],%u%*[\r]", &profile_id, &http_response, content_type.data(), &response_length)) {
		debug_if(Debug_Print, "Did not get a valid ring\r\n");
		return Result::Timeout;
	}
	debug_if(Debug_Print, "HTTP status:%u, length: %d\r\n", http_response, response_length);
	if (http_response != 200) {
		debug_if(Debug_Print, "Did not receive a 200, we got %u \r\n", http_response);
		return Result::Error;
	}

	if (response.size() != response_length) {
		debug_if(Debug_Print, "Error expected %u but got %d\r\n", response.size(), response_length);
		return Result::Error;
	}

	if (response_length != 0)
	{
		_parser.send("AT#HTTPRCV=%d", profile_id);
		if (!(_parser.recv("<<<")
		      && _parser.read(response)
		      && _parser.recv("OK")))
		{
			return Result::Timeout;
		}
	}
	return Result::OK;
}

int8_t Telit::get_rssi() {
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	_parser.send("AT+CSQ");
	int rssi;
	int ber;
	if (_parser.recv("+CSQ: %d,%d", &rssi, &ber)
	    && _parser.recv("OK"))
	{
		debug_if(Debug_Print, "RSSI:%d, BER:%d\r\n", (rssi * 2 - 113), ber);
		return rssi;
	}
	return -1;
}

Telit::Result Telit::wait_for_network_reg(std::chrono::milliseconds timeout) {
	Auto_Timer timer;
	while (!(_reg_state == Registration_State::Home_Network
	       || _reg_state == Registration_State::Roaming))
	{
		if (timer.read() > timeout) {
			return Result::Timeout;
		}
		thread_wait(std::chrono::milliseconds(500));
	}
	return Result::OK;
}

void Telit::reset() {
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	debug_if(Debug_Print, "Powering off modem\r\n");
	thread_wait(std::chrono::seconds(5));
	_parser.send("AT#ENHRST=1,0");
	_parser.recv("OK");
	_state = Modem_State::Uninitialised;
	thread_wait(std::chrono::seconds(10));
}

Telit::Result Telit::init_gps() {
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	debug_if(Debug_Print, "Enabling GPS\r\n");
	thread_wait(std::chrono::milliseconds(1000));
	//-- Disable all unsolicited messages
	_parser.send("AT$GPSNMUN=0,0,0,0,0,0,0");
	if (!_parser.recv("OK"))
	{
		debug_if(Debug_Print, "Could not disable gps unsolicited messages\r\n");
		return Result::Error;
	}
	thread_wait(std::chrono::milliseconds(1000));
	//-- Enable the gps
	int gps_state;
	_parser.send("AT$GPSP?");
	if (_parser.recv("$GPSP: %d", &gps_state)
	    && _parser.recv("OK")) {
	}
	if (gps_state == 0) {
		_parser.send("AT$GPSP=1");
		if (!_parser.recv("OK")) {
			debug_if(Debug_Print, "Could not enable the gps\r\n");
			return Result::Error;
		}
	}
	thread_wait(std::chrono::milliseconds(1000));
	debug_if(Debug_Print, "GPS is Running\r\n");
	return Result::OK;
}

Telit::Gps_Location Telit::get_location() {
	Gps_Location result;
	std::lock_guard<rtos::Mutex> lk(_at_mutex);
	_parser.setTimeout(std::chrono::milliseconds(1000));
	_parser.send("AT$GPSACP");
	char str_time[11], str_lat[11], str_long[11];
	if (_parser.recv("$GPSACP: %10[^,],%10[^,],%10[^,],", str_time, str_lat, str_long)
	    && _parser.recv("OK"))
	{
		/* <UTC> - UTC time (hhmmss.sss) referred to GGA sentence
		* <latitude> - format is ddmm.mmmm N/S (referred to GGA sentence) where:
		* 		dd - degrees 00..90
		* 		mm.mmmm - minutes 00.0000..59.9999
		* 		N/S: North / South
		* <longitude> - format is dddmm.mmmm E/W (referred to GGA sentence) where:
		* 		ddd - degrees 000..180
		*	 	mm.mmmm - minutes 00.0000..59.9999
		*	 	E/W: East / West
		*/
		result.latitude = (str_lat[0] - '0') * 10 + (str_lat[1] - '0');
		float minutes = strtof(str_lat + 2, nullptr);
		result.latitude += minutes / 60.f;
		if (str_lat[9] == 'S') {
			result.latitude *= -1;
		}

		result.longitude = (str_long[0] - '0') * 100 + (str_long[1] - '0') * 10 + (str_long[2] - '0');
		minutes = strtof(str_long + 3, nullptr);
		result.longitude += minutes / 60.f;
		if (str_lat[9] == 'W') {
			result.longitude *= -1;
		}

		debug_if(Debug_Print, "Got a GPS wrong: %s, %s", str_lat, str_long);
		debug_if(Debug_Print, "Got a GPS fix: %f,%f\r\n", result.latitude, result.longitude);
		result.res = Result::OK;
	}
	else {
		result.res = Result::Error;
	}
	return result;
}

void Telit::Urc_CGREG_Handler()
{
	constexpr String_Ref urc_cgreg("+CGREG: ");  // has been received
	int registration = -1;
	int tmp;
	_parser.recv("%100[^\r]%*[\r]", tmp_buff);
	// if this is the urc, there is only 1 digit.
	// if this is the read command, the first digit is the cgreg mode and needs ignoring
	if(2 == sscanf(tmp_buff, "%d,%d", &registration, &tmp)) {
		registration = tmp;
	}
	// valid registration states are in the range of 0-5
	// see 'Registration_States' for more context
	if(registration >= 0
	    && registration <= 5)
	{
		_reg_state = static_cast<Registration_State>(registration);
	}
}

void Telit::Urc_CGEV_Handler()
{
	constexpr String_Ref urc_cgerep("+CGEV: ");
	{
		_parser.recv("%100[^\r]%*[\r]", tmp_buff);
	}
	// the first 6 characters are enough to identify which URC this is
	String_Ref received(tmp_buff, 6);

	if (received == "REJECT") {
		 // PDP activation request rejected
	    int context_id;
		sscanf(tmp_buff, "REJECT %d,", &context_id);
		// context id activation has been rejected
		_PDP_Activation_States[context_id - 1] = false;
	}
	else if (received == "NW REA") {
		 // NW REACT: nw has requested a PDP context reactivation
	    int context_id;
		sscanf(tmp_buff, "NW REACT %d,", &context_id);
		// context id has been reactivated
		debug_if(Debug_Print, "reactivated PDP: %d\r\n", context_id);
	}
	else if (received == "NW DEA") {
		 // NW DEACT: nw has deativated the PDP context
	    int context_id;
		sscanf(tmp_buff, "NW DEACT %d,", &context_id);
		// context id is deactivated
		_PDP_Activation_States[context_id - 1] = false;
	}
	else if (received == "ME DEA") {
		 // ME DEACT: modem has deactivated the PDP context
	    int context_id;
		sscanf(tmp_buff, "ME DEACT %d,", &context_id);
		// context id is deactivated
		_PDP_Activation_States[context_id - 1] = false;
	}
	else if (received == "NW DET" // NW DETACH: network detached, all PDP contexts deactivated
	         || received == "ME DET") // ME DETACH: network detached, all PDP contexts deactivated
		{
			// all contexts are deactivated
			// gprs detached
			for(bool & state : _PDP_Activation_States) {
				state = false;
			}
			_GPRS_Attached = false;
		}
	else {
		debug_if(Debug_Print, "+CGEV unhandled: %s", tmp_buff);
	}
	//else if (received == make_carray_ref("ME CLA")) { // ME CLASS: modem changed MS class
	//}
}


void Telit::Urc_CME_ERROR_Handler()
{
	constexpr String_Ref urc_cmee("+CME ERROR: ");
	_parser.recv("%100[^\r]%*[\r]", tmp_buff);
	debug_if(Debug_Print, "CME ERROR: %s\r\n", tmp_buff);
}
