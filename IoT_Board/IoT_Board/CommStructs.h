/*
 * cs_mf_types.h
 *
 *  Created on: 15/08/2017
 *      Author: Tobin
 */

#ifndef MEAL_SRC_CS_MF_TYPES_H_
#define MEAL_SRC_CS_MF_TYPES_H_
#include <cstdint>

struct status_t {
	uint64_t timestamp;
	uint64_t uptime;
	uint32_t solar_panel_power;
	uint32_t solar_generated_energy;
	uint32_t solar_consumed_energy;
	uint16_t solar_controller_temp;
	uint16_t battery_level;
	uint8_t hopper_level;
	uint8_t lockout;
	uint8_t hatch_open;
};

struct calf_info_t {
	uint64_t rfid;
	uint64_t timestamp;
	uint8_t station;
};

struct feed_info_t {
	uint64_t rfid;
	uint64_t timestamp;
	uint16_t auger_time;
	uint8_t auger_units;
	uint8_t station;
	uint16_t sensor_calibration[4];
};


#endif /* MEAL_SRC_CS_MF_TYPES_H_ */

