#pragma once
#ifndef ARRAY_REF_H
#define ARRAY_REF_H

#include <string>
#include <algorithm>

template<typename T>
	class Array_Ref
	{
	public:
		constexpr Array_Ref(std::nullptr_t)
			: _start(nullptr)
			, _length(0u)
		{}

		// implicit construction from a static raw C array
		template<std::size_t N>
		    constexpr Array_Ref(const T(& char_arr)[N]) // brackets bind the reference to the name, not the array elements
		        : _start(char_arr)
		        , _length(N)
		{}

		// construct from char* + length
		constexpr explicit Array_Ref(T* from, std::size_t size)
			: _start(from)
			, _length(size)
		{}

		// construct Array_Ref<const T> from Array_Ref<T>
		template<typename U,
		    typename = std::enable_if_t< // enable if
		        std::is_same<std::remove_const_t<T>, std::remove_const_t<U>>::value // T and U are the same type (ignoring const qualifiers)
		        && std::is_const<T>::value && !std::is_const<U>::value>> // && T is const && U is not const
		constexpr Array_Ref(Array_Ref<U> ref)
			: _start(ref.data())
			, _length(ref.size())
		{ }

	// investigate later:
	// default copy/assignment was corrupting the variable
	// since this should be a straight memcpy for array ref copy/move shouldn't be required
	// since they were, all 5 special member functions have been added
	constexpr Array_Ref(const Array_Ref& ref)
	    : _start(ref._start)
			, _length(ref._length)
		{
		}

		constexpr Array_Ref(Array_Ref&& ref)
			: _start(std::move(ref._start))
			, _length(std::move(ref._length))
		{
		}

		constexpr Array_Ref& operator=(const Array_Ref& ref)
		{
			_start = ref._start;
			_length = ref._length;
			return *this;
		}

		constexpr Array_Ref& operator=(Array_Ref&& ref)
		{
			_start = std::move(ref._start);
			_length = std::move(ref._length);
			return *this;
		}

		~Array_Ref() = default;

		// End special member function implementations

		constexpr T& operator[](int index)
		{
			return _start[index];
		}

		constexpr const T& operator[](int index) const
		{
			return _start[index];
		}

		constexpr T* data()
		{
			return _start;
		}

		constexpr const T* data() const
		{
			return _start;
		}

		constexpr std::size_t size() const
		{
			return _length;
		}

		constexpr T* begin()
		{
			return data();
		}

		constexpr const T* begin() const
		{
			return data();
		}

		constexpr const T* end() const
		{
			return data() + _length;
		}

	private:
		T* _start;
		std::size_t _length;
	};

class String_Ref : public Array_Ref<const char>
{
public:
	// implicit construction from a static raw char array
template<std::size_t N>
    constexpr String_Ref(const char(& char_arr)[N]) // brackets bind the reference to the name, not the array elements
        : Array_Ref(char_arr)
	{}

	// construct from char* + length
	constexpr explicit String_Ref(const char* from, std::size_t size)
		: Array_Ref(from, size)
	{}

	explicit String_Ref(const std::string& str)
		: Array_Ref(str.c_str(), str.size())
	{}
};

namespace detail
{
	// neither of std::equal and strcmp is constexpr, need our own impl
	template<typename Iter>
	constexpr bool equal(Iter begin1, Iter end1, Iter begin2, Iter end2)
	{
		while (begin1 != end1 && begin2 != end2)
		{
			if (*begin1 != *begin2) {
				return false;
			}
			++begin1;
			++begin2;
		}
		return begin1 == end1 && begin2 == end2;
	}
}

template<typename T>
	constexpr bool operator ==(Array_Ref<T> lhs, Array_Ref<T> rhs)
	{
		return lhs.size() == rhs.size() &&::detail::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

template<typename T>
	constexpr bool operator !=(Array_Ref<T> lhs, Array_Ref<T> rhs)
	{
		return !(lhs == rhs);
	}

template<std::size_t N>
	constexpr bool operator ==(String_Ref lhs, char(&rhs)[N])
	{
		return lhs == String_Ref(rhs);
	}

template<std::size_t N>
	constexpr bool operator ==(char(&lhs)[N], String_Ref rhs)
	{
		return String_Ref(lhs) == rhs;
	}

inline bool operator ==(const std::string& lhs, String_Ref rhs)
{
	return String_Ref(lhs) == rhs;
}

inline bool operator ==(String_Ref lhs, const std::string& rhs)
{
	return lhs == String_Ref(rhs);
}

template<typename T, std::size_t N, typename arr_t = std::remove_const_t<T>>
	constexpr Array_Ref<arr_t> make_array_ref(T(& arr)[N])
	{
		return Array_Ref<arr_t>(arr);
	}

template<typename T, typename arr_t = std::remove_const_t<T>>
	constexpr Array_Ref<arr_t> make_array_ref(T* arr_begin, std::size_t arr_size)
	{
		return Array_Ref<arr_t>(arr_begin, arr_size);
	}

template<typename T, std::size_t N, typename arr_t = std::remove_const_t<T>>
	constexpr Array_Ref<const arr_t> make_carray_ref(T(& arr)[N])
	{
		return Array_Ref<const arr_t>(arr);
	}

template<typename T, typename arr_t = std::remove_const_t<T>>
	constexpr Array_Ref<const arr_t> make_carray_ref(T* arr_begin, std::size_t arr_size)
	{
		return Array_Ref<const arr_t>(arr_begin, arr_size);
		;
	}

template<typename T>
	constexpr Array_Ref<T> sub_ref(Array_Ref<T> ref, int pos, int length)
	{
		return Array_Ref<T>(ref.begin() + pos, length);
	}

#endif
